//
//  login-constants.swift
//  CurrenciesConverter
//
//  Created by Ashish Patel on 2019/07/13.
//  Copyright © 2019 Ashish Patel. All rights reserved.
//

import Foundation

let API_GET_LATEST_EXCHANGE_RATE:String = "https://europe-west1-revolut-230009.cloudfunctions.net/revolut-ios"

let ADD_CURRENCY_PAIR:String = "Add currency pair"
let RATES_CONVERTER:String = "Rates & converter"
let CHOSSE_A_CURRENCY:String = "Chosse a currency pair to compare their live rates"
