//
//  ShareObject.swift
//  CurrenciesConverter
//
//  Created by Ashish Patel on 2019/02/10.
//  Copyright © 2019. All rights reserved.
//

import UIKit

class SharedClass: NSObject {
    static let sharedInstance = SharedClass()
    let loader = CustomLoader(title: "")
    var isLocationReceived = false
    func showLoader(strTitle:String) {
        DispatchQueue.main.async {
            self.loader.show(animated: true)
        }
    }
    
    func dismissLoader() {
        DispatchQueue.main.async {
            self.loader.dismiss(animated: true)
        }
    }
}
