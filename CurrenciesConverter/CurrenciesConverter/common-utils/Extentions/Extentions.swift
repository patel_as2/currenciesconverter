//
//  AppUtils.swift
//  CurrenciesConverter
//
//  Created by Ashish Patel on 2019/07/13.
//  Copyright © 2019 Ashish Patel. All rights reserved.
//

import Foundation
import UIKit

struct FontHelper {
    static func defaultRegularRobotoFontWithSize(size: CGFloat) -> UIFont {
        return UIFont(name: "Roboto", size: size)!
    }
    
    static func mediumRobotoFontWithSize(size: CGFloat) -> UIFont {
        return UIFont(name: "Roboto-Medium", size: size)!
    }
}

extension UIColor {
    
    class func backgroundWhiteColor() -> UIColor {
        return UIColor(red: 0.95, green: 0.95, blue: 0.95, alpha: 1)
    }
    
    class func buleTextColor() -> UIColor {
        return UIColor(red: 0, green: 0.46, blue: 0.92, alpha: 1)
    }
    
    class func blackTextColor() -> UIColor {
        return UIColor(red: 0.1, green: 0.11, blue: 0.12, alpha: 1)
    }

    class func lightGrayTextColor() -> UIColor {
        return UIColor(red: 1.83, green: 1.71, blue: 1.61, alpha: 1)
    }
}

extension UIButton {
    
    func centerImageAndButton(_ gap: CGFloat, imageOnTop: Bool) {
        
        guard let imageView = self.currentImage,
            let titleLabel = self.titleLabel?.text else { return }
        
        let sign: CGFloat = imageOnTop ? 1 : -1
        self.titleEdgeInsets = UIEdgeInsets(top: (imageView.size.height + gap) * sign, left: -imageView.size.width, bottom: 0, right: 0);
        
        let titleSize = titleLabel.size(withAttributes:[NSAttributedString.Key.font: self.titleLabel!.font!])
        self.imageEdgeInsets = UIEdgeInsets(top: -(titleSize.height + gap) * sign, left: 0, bottom: 0, right: -titleSize.width)
    }
}
extension UIView {
    func fadeTo(_ alpha: CGFloat, duration: TimeInterval = 0.3) {
        DispatchQueue.main.async {
            UIView.animate(withDuration: duration) {
                self.alpha = alpha
            }
        }
    }
    
    func fadeIn(_ duration: TimeInterval = 0.3) {
        fadeTo(1.0, duration: duration)
    }
    
    func fadeOut(_ duration: TimeInterval = 0.3) {
        fadeTo(0.0, duration: duration)
    }
}

extension UIImageView {
    func makeRounded() {
        self.layer.cornerRadius = (self.frame.width / 2)
        self.layer.masksToBounds = true
        
    }
}

extension UITableView {
    
    func setEmptyView(title: String, message: String, messageImage: UIImage, target: Any, action: Selector) {
        
        let emptyView = UIView(frame: CGRect(x: self.center.x, y: self.center.y, width: self.bounds.size.width, height: self.bounds.size.height))
        
        let btnTitleWithImage = UIButton(type: .custom)
        btnTitleWithImage.backgroundColor = .clear
        btnTitleWithImage.setTitleColor(UIColor.buleTextColor(), for: .normal)
        btnTitleWithImage.titleLabel?.font = FontHelper.mediumRobotoFontWithSize(size: 16)
        
        let messageLabel = UILabel()
        messageLabel.textColor = UIColor.lightGray
        messageLabel.font = FontHelper.defaultRegularRobotoFontWithSize(size: 16)
        messageLabel.text = message
        messageLabel.numberOfLines = 0
        messageLabel.textAlignment = .center
        
        emptyView.addSubview(btnTitleWithImage)
        emptyView.addSubview(messageLabel)
        
        btnTitleWithImage.setImage(messageImage, for: .normal)
        btnTitleWithImage.setTitle(title, for: .normal)
        btnTitleWithImage.addTarget(target, action: action, for: .touchUpInside)
        btnTitleWithImage.alpha = 0.0
        btnTitleWithImage.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        btnTitleWithImage.contentHorizontalAlignment
            = .center
        
        
        btnTitleWithImage.translatesAutoresizingMaskIntoConstraints = false
        btnTitleWithImage.centerXAnchor.constraint(equalTo: emptyView.centerXAnchor).isActive = true
        btnTitleWithImage.centerYAnchor.constraint(equalTo: emptyView.centerYAnchor, constant: -20).isActive = true
        btnTitleWithImage.widthAnchor.constraint(equalToConstant: self.bounds.size.width).isActive = true
        btnTitleWithImage.heightAnchor.constraint(equalToConstant: 80).isActive = true
        
        messageLabel.translatesAutoresizingMaskIntoConstraints = false
        messageLabel.topAnchor.constraint(equalTo: btnTitleWithImage.bottomAnchor, constant: 5).isActive = true
        messageLabel.widthAnchor.constraint(equalToConstant: self.bounds.size.width-130).isActive = true
        messageLabel.centerXAnchor.constraint(equalTo: emptyView.centerXAnchor).isActive = true
        
        btnTitleWithImage.centerImageAndButton(10, imageOnTop: true)
        btnTitleWithImage.fadeTo(1.0, duration: 1.5)
        
        self.backgroundView = emptyView
    }
    
    func restore() {
        self.backgroundView = nil
    }
}
