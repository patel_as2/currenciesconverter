//
//  ViewToPresenterSelectCurrencyProtocol.swift
//  CurrenciesConverter
//
//  Created by Ashish Patel on 2019/07/13.
//  Copyright © 2019 Ashish Patel. All rights reserved.
//

import Foundation
import UIKit

protocol ViewToPresenterSelectCurrencyProtocol:class{
    
    var view: PresenterToViewSelectCurrencyProtocol? {get set}
    var interactor: PresenterToInteractorSelectCurrencyProtocol? {get set}
    var router: PresenterToRouterSelectCurrencyProtocol? {get set}
    func startFetchingCurrencies()
    func animateSelectCurrencyController(viewController:UIViewController)
    func addCurrencyPair(objCurrencyPair: CurrencyPairModel)
}

protocol PresenterToViewSelectCurrencyProtocol:class {
    
    func onSelectCurrencyResponseSuccess(currencyModelArrayList:Array<SelectCurrencyModel>)
    func onAddCurrencyResponse(result:Bool)
}

protocol PresenterToRouterSelectCurrencyProtocol:class {
    
    func createSelectCurrencyModule(arySelectedCurrenciesPair:Array<CurrencyPairModel>) -> selectCurrencyVC
    func animateSelectCurrencyScreen(viewController: UIViewController)
}

protocol PresenterToInteractorSelectCurrencyProtocol:class {
    
    var presenter:InteractorToPresenterSelectCurrencyProtocol? {get set}
    func fetchCurrencyList()
    func addCurrencyPair(objCurrencyPair:CurrencyPairModel)
    
}

protocol InteractorToPresenterSelectCurrencyProtocol:class {
    
    func selectCurrencyFetchSuccess(currenciesList:Array<SelectCurrencyModel>)
    func addCurrencyPairSuccess(result:Bool)

}
