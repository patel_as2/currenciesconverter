//
//  selectCurrencyVC.swift
//  CurrenciesConverter
//
//  Created by Ashish Patel on 2019/07/13.
//  Copyright © 2019 Ashish Patel. All rights reserved.
//

import UIKit

class CurrencyCell:UITableViewCell{
    @IBOutlet weak var imgCurrencyLogo: UIImageView!
    @IBOutlet weak var lblCurrencyTitle: UILabel!
    @IBOutlet weak var lblCurrencyCode: UILabel!
    
    func setupCellValue(modelCurrency:SelectCurrencyModel) {
        if let code = modelCurrency.currencyCode, let title = modelCurrency.title {
            self.lblCurrencyCode.text = code
            self.lblCurrencyTitle.text = title }
        if let imageCourtryName = modelCurrency.imagesource, let imageCountry = UIImage(named: imageCourtryName) {
            self.imgCurrencyLogo.image = imageCountry
        }
    }
}

class selectCurrencyVC: UIViewController {

    var selectCurrencyPresenter:ViewToPresenterSelectCurrencyProtocol?
    var isSourceCurrencySelected = false
    @IBOutlet weak var tblSelectCurrency: UITableView!{
        didSet {
            tblSelectCurrency.delegate = self
            tblSelectCurrency.dataSource = self
        }
    }
    var aryCurrency:Array<SelectCurrencyModel> = Array()
    var arySelectedCurrenciesPair:Array<CurrencyPairModel> = Array()

    override func viewDidLoad() {
        super.viewDidLoad()
        selectCurrencyPresenter?.startFetchingCurrencies()
    }
    
    override func viewWillAppear(_ animated: Bool) {

    }
    func filerSelectedItem(modelCurrency:SelectCurrencyModel) -> Bool {
        let objCurrentCurrencyPair = arySelectedCurrenciesPair.last
        let aryData = self.arySelectedCurrenciesPair.filter {($0.sourceCurrencyCode == objCurrentCurrencyPair?.sourceCurrencyCode)}
        let result = aryData.filter{($0.destinationCurrencyCode == modelCurrency.currencyCode) || (objCurrentCurrencyPair?.sourceCurrencyCode == modelCurrency.currencyCode)}
        return result.count > 0
    }
    
}

extension selectCurrencyVC:PresenterToViewSelectCurrencyProtocol{
    func onAddCurrencyResponse(result:Bool) {
        self.dismiss(animated: true, completion: nil)
    }

    func onSelectCurrencyResponseSuccess(currencyModelArrayList:Array<SelectCurrencyModel>) {
        self.aryCurrency = currencyModelArrayList
        self.tblSelectCurrency.reloadData()
    }
}

extension selectCurrencyVC:UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aryCurrency.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CurrencyCell") as! CurrencyCell
        cell.setupCellValue(modelCurrency:aryCurrency[indexPath.row] as SelectCurrencyModel)
        if isSourceCurrencySelected && filerSelectedItem(modelCurrency:aryCurrency[indexPath.row] as SelectCurrencyModel){
            cell.isUserInteractionEnabled = false
            cell.contentView.alpha = 0.5
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tblSelectCurrency.deselectRow(at: indexPath, animated: true)
        let objSelectedCurrency = aryCurrency[indexPath.row] as SelectCurrencyModel
        if !isSourceCurrencySelected {
            isSourceCurrencySelected = true
            selectCurrencyPresenter?.animateSelectCurrencyController(viewController: self)
            let objCurrencyPair = CurrencyPairModel()
            objCurrencyPair.pid = objCurrencyPair.incrementID()
            objCurrencyPair.sourceCurrencyName = objSelectedCurrency.title
            objCurrencyPair.sourceCurrencyCode = objSelectedCurrency.currencyCode
            arySelectedCurrenciesPair.append(objCurrencyPair)
            tblSelectCurrency.reloadSections(NSIndexSet.init(index: 0) as IndexSet, with: UITableView.RowAnimation.fade)
        }
        else {
            if arySelectedCurrenciesPair.count > 0 {
                if let objCurrencyPair = arySelectedCurrenciesPair.last {
                _ = arySelectedCurrenciesPair.dropLast()
                objCurrencyPair.destinationCurrencyName = objSelectedCurrency.title
                objCurrencyPair.destinationCurrencyCode = objSelectedCurrency.currencyCode
                objCurrencyPair.exchangePair = "\(objCurrencyPair.sourceCurrencyCode!)\(objSelectedCurrency.currencyCode!)"
                arySelectedCurrenciesPair.append(objCurrencyPair)
                selectCurrencyPresenter?.addCurrencyPair(objCurrencyPair: objCurrencyPair)
                }
            }
        }
        
    }
    
}
