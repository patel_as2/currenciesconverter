//
//  SelectCurrencyModel.swift
//  CurrenciesConverter
//
//  Created by Ashish Patel on 2019/07/13.
//  Copyright © 2019 Ashish Patel. All rights reserved.

import Foundation
import ObjectMapper

private let Code = "Code"
private let Title = "Name"
private let IMAGESOURCE = "Code"
@objcMembers
class SelectCurrencyModel:Mappable{
    
    internal var currencyCode:String?
    internal var title:String?
    internal var imagesource:String?
    
    required init?(map:Map) {
        mapping(map: map)
    }
    
    func mapping(map:Map){
        currencyCode <- map[Code]
        title <- map[Title]
        imagesource <- map[IMAGESOURCE]
    }
    
}
