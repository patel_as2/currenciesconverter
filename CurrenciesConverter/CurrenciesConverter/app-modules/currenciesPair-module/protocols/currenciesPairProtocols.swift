//
//  currenciesPairProtocols.swift
//  CurrenciesConverter
//
//  Created by Ashish Patel on 2019/07/13.
//  Copyright © 2019 Ashish Patel. All rights reserved.
//

import Foundation
import UIKit

protocol ViewToPresenterProtocol: class{
    
    var view: PresenterToViewProtocol? {get set}
    var interactor: PresenterToInteractorProtocol? {get set}
    var router: PresenterToRouterProtocol? {get set}
    func startFetchingCurrenciesPair()
    func showSelectCurrencyController(navigationController:UINavigationController,currenciesPair:Array<CurrencyPairModel>)

}

protocol PresenterToViewProtocol: class{
    func showCurrenciesPair(currenciesPairArray:Array<CurrencyPairModel>)
}

protocol PresenterToRouterProtocol: class {
    func createModule()-> currenciesPairVC
    func pushToSelectCurrencyScreen(navigationConroller:UINavigationController,arySelectedCurrenciesPair:Array<CurrencyPairModel>)
    
}

protocol PresenterToInteractorProtocol: class {
    var presenter:InteractorToPresenterProtocol? {get set}
    func getLatestExchangeRates()
}

protocol InteractorToPresenterProtocol: class {
    func currenciesPairFetchedSuccess(currenciesPairModelArray:Array<CurrencyPairModel>)
}
