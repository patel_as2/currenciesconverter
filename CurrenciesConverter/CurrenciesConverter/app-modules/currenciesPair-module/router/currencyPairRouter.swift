//
//  currencyPairRouter.swift
//  CurrenciesConverter
//
//  Created by Ashish Patel on 2019/07/13.
//  Copyright © 2019 Ashish Patel. All rights reserved.
//

import Foundation
import UIKit

class currencyPairRouter:PresenterToRouterProtocol{
    
    func createModule() -> currenciesPairVC {
        
        let view = mainstoryboard.instantiateViewController(withIdentifier: "MyViewController") as! currenciesPairVC
        
        let presenter: ViewToPresenterProtocol & InteractorToPresenterProtocol = currenciesPairPresenter()
        let interactor: PresenterToInteractorProtocol = currencyPairInteractor()
        let router:PresenterToRouterProtocol = currencyPairRouter()
        
        view.presentor = presenter
        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor
        interactor.presenter = presenter
        
        return view
        
    }
    
     var mainstoryboard: UIStoryboard{
        return UIStoryboard(name:"Main",bundle: Bundle.main)
    }
    
    func pushToSelectCurrencyScreen(navigationConroller: UINavigationController, arySelectedCurrenciesPair: Array<CurrencyPairModel>) {
        let selectCurrencyModue = selectCurrencyRouter().createSelectCurrencyModule(arySelectedCurrenciesPair: arySelectedCurrenciesPair)
        navigationConroller.present(selectCurrencyModue, animated: true, completion: nil)
    }
    
}
