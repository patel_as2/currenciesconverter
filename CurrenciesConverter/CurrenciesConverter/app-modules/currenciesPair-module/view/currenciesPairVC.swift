//
//  ViewController.swift
//  CurrenciesConverter
//
//  Created by Ashish Patel on 2019/07/13.
//  Copyright © 2019 Ashish Patel. All rights reserved.
//

import UIKit
import RealmSwift

class currenciesPairCell:UITableViewCell{
    @IBOutlet weak var lblSourceCurrencyName: UILabel!
    @IBOutlet weak var lblSourceCurrencyRate: UILabel!
    @IBOutlet weak var lblDestinationCurrencyName: UILabel!
    @IBOutlet weak var lblDestinationCurrencyRate: UILabel!
}

class currenciesPairVC: UIViewController {

    var presentor:ViewToPresenterProtocol?
    var fetchExchangeTimer = Timer()
    var aryCurrenciesPair:Array<CurrencyPairModel> = Array()
    var isTblCurrenciesEditing = false
    let CELL_HEIGHT : CGFloat = 72

    @IBOutlet weak var tblCurrenciesData: UITableView! {
        didSet {
            tblCurrenciesData.delegate = self
            tblCurrenciesData.dataSource = self
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = RATES_CONVERTER
        self.view.backgroundColor = UIColor.backgroundWhiteColor()
        SharedClass.sharedInstance.showLoader(strTitle: "")
    }
    
    override func viewWillAppear(_ animated: Bool) {
       fetchExchangeTimer = Timer.scheduledTimer(timeInterval: 1.0,
                                     target: self,
                                     selector: #selector(getCurrenciesPair),
                                     userInfo: nil,
                                     repeats: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        fetchExchangeTimer.invalidate()
    }
    
    @objc func getCurrenciesPair() {
        presentor?.startFetchingCurrenciesPair()
    }
    
    @objc func addCurrencyButtonPressed() {
        presentor?.showSelectCurrencyController(navigationController: navigationController!, currenciesPair: aryCurrenciesPair)
    }
    
    func realmDelete(code: String) {
        do {
            let realm = try Realm()
            let object = realm.objects(CurrencyPairModel.self).filter("exchangePair = %@", code).first
            try! realm.write {
                if let obj = object {
                    realm.delete(obj)
                }
                realm.refresh()
            }
        } catch let error as NSError {
            // handle error
            print("error - \(error.localizedDescription)")
        }
    }
    
    func setFormattedMoney(amount:String,label:UILabel) {
        if let number = Double(amount) {
            let formatter = NumberFormatter()
            formatter.numberStyle = .decimal
            formatter.maximumFractionDigits = 4
            formatter.roundingMode = .up
            
            var str = String(describing: formatter.string(from: number as NSNumber)!)
            str = str.replacingOccurrences(of: ".", with: ",")
            let myMutableString = NSMutableAttributedString(string: str)
            let ary = str.components(separatedBy: ",")
            if ary.count == 2 {
                let split = ary.last
                if split!.count == 4 {
                    myMutableString.addAttribute(NSAttributedString.Key.font,
                                                 value: FontHelper.defaultRegularRobotoFontWithSize(size: 14),
                                                 range: NSRange(
                                                    location: str.count-2,
                                                    length: 2))
                }
                label.attributedText = myMutableString
            }
        }
        
    }
}

extension currenciesPairVC:PresenterToViewProtocol{
    func showCurrenciesPair(currenciesPairArray: Array<CurrencyPairModel>) {
        SharedClass.sharedInstance.dismissLoader()
        if !isTblCurrenciesEditing && currenciesPairArray.count > 0 {
            if self.aryCurrenciesPair.count > 0 && self.aryCurrenciesPair.count < currenciesPairArray.count{
                self.aryCurrenciesPair = currenciesPairArray
                tblCurrenciesData.insertRows(at: [IndexPath(row: 0, section: 0)], with: UITableView.RowAnimation.fade)
            }
            else {
                self.aryCurrenciesPair = currenciesPairArray
                self.tblCurrenciesData.reloadData()
            }
        }
    }
}

extension currenciesPairVC:UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if aryCurrenciesPair.count == 0 {
            if let imagePlus = UIImage(named: "Plus") {
                tableView.setEmptyView(title: ADD_CURRENCY_PAIR, message: CHOSSE_A_CURRENCY, messageImage: imagePlus, target: self, action: #selector(addCurrencyButtonPressed))
                self.navigationItem.title = ""
            }
        }
        else {
            self.navigationItem.title = RATES_CONVERTER
            tableView.restore()
        }
        return aryCurrenciesPair.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "currenciesPairCell") as! currenciesPairCell
        
        let objCurrencyPair = aryCurrenciesPair[indexPath.row] as CurrencyPairModel
        if let sourceCurrencyCode = objCurrencyPair.sourceCurrencyCode,let sourceCurrencyName = objCurrencyPair.sourceCurrencyName,let destinationCurrencyName = objCurrencyPair.destinationCurrencyName,let destinationCurrencyCode = objCurrencyPair.destinationCurrencyCode {
        cell.lblSourceCurrencyRate.text = "1 \(sourceCurrencyCode)"
        cell.lblSourceCurrencyName.text = sourceCurrencyName
        cell.lblDestinationCurrencyName.text = "\(destinationCurrencyName) - \(destinationCurrencyCode)"
        cell.lblDestinationCurrencyRate.text = objCurrencyPair.currencyExchangeRate
        setFormattedMoney(amount: objCurrencyPair.currencyExchangeRate!, label: cell.lblDestinationCurrencyRate)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if aryCurrenciesPair.count == 0 {
            return 0
        }
        return CELL_HEIGHT
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView.init(frame: CGRect.init(x: self.tblCurrenciesData.frame.origin.x, y: self.tblCurrenciesData.frame.origin.y, width: self.tblCurrenciesData.bounds.size.width, height:72))
        headerView.backgroundColor = .white
        let btnTitleWithImage = UIButton(type: .custom)
        btnTitleWithImage.backgroundColor = .clear
        btnTitleWithImage.setTitleColor(UIColor.buleTextColor(), for: .normal)
        btnTitleWithImage.titleLabel?.font = FontHelper.mediumRobotoFontWithSize(size: 16)
        headerView.addSubview(btnTitleWithImage)
        btnTitleWithImage.translatesAutoresizingMaskIntoConstraints = false
        btnTitleWithImage.addTarget(self, action: #selector(addCurrencyButtonPressed), for: .touchUpInside)
        btnTitleWithImage.leadingAnchor.constraint(equalTo: headerView.leadingAnchor, constant:16).isActive = true
        btnTitleWithImage.topAnchor.constraint(equalTo: headerView.topAnchor, constant:16).isActive = true
        btnTitleWithImage.widthAnchor.constraint(equalToConstant: headerView.bounds.size.width).isActive = true
        btnTitleWithImage.heightAnchor.constraint(equalToConstant: 40).isActive = true

        if let imagePlus = UIImage(named: "Plus") {
            btnTitleWithImage.setImage(imagePlus, for: .normal)}
        btnTitleWithImage.setTitle(ADD_CURRENCY_PAIR, for: .normal)
        btnTitleWithImage.titleEdgeInsets = UIEdgeInsets(top: 0, left: 17, bottom: 0, right: 0)
        btnTitleWithImage.contentHorizontalAlignment
            = .left
        btnTitleWithImage.contentVerticalAlignment = .center
        return headerView
    }
    
     func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        isTblCurrenciesEditing = true
        let delete = UIContextualAction(style: .destructive, title: "Delete") { (action, sourceView, completionHandler) in
            print("index path of delete: \(indexPath)")
            let objCurrency = self.aryCurrenciesPair[indexPath.row]
            self.realmDelete(code: objCurrency.exchangePair!)
            self.aryCurrenciesPair.remove(at: indexPath.row)
            self.tblCurrenciesData.deleteRows(at: [indexPath], with: .automatic)
            self.isTblCurrenciesEditing = false
            completionHandler(true)
        }
        let swipeActionConfig = UISwipeActionsConfiguration(actions: [delete])
        swipeActionConfig.performsFirstActionWithFullSwipe = false
        return swipeActionConfig
    }
    
}



