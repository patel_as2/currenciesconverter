//
//  currencyPairInteractor.swift
//  CurrenciesConverter
//
//  Created by Ashish Patel on 2019/07/13.
//  Copyright © 2019 Ashish Patel. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper
import RealmSwift

class currencyPairInteractor: PresenterToInteractorProtocol{
    var presenter: InteractorToPresenterProtocol?
    
    internal func fetchCurrencyPair() -> Array<CurrencyPairModel>{
        var aryObjects = Array<CurrencyPairModel>()
        RealmManager.fetch(model: CurrencyPairModel.className(), condition: nil, completionHandler: { (result) in
            if result.count > 0 {
                aryObjects = result.compactMap{$0 as? CurrencyPairModel}
            }
        })
        return aryObjects
    }
    
    internal func getLatestExchangeRatesAPIParms() -> [(String, String)]{
        let aryCurrencyPair = fetchCurrencyPair()
        if Reachability.isConnectedToNetwork() && aryCurrencyPair.count > 0{
            let params = aryCurrencyPair.compactMap { object -> (String, Any) in
                return ("pairs",object.exchangePair ?? "")
            }
            return params as! [(String, String)]

        }
        return [(String, String)]()
    }
    
    func getLatestExchangeRates() {
        let params = getLatestExchangeRatesAPIParms()
        if Reachability.isConnectedToNetwork() && params.count > 0 {
        let urlComponent = setUrlPathComponent(url: URL(string:API_GET_LATEST_EXCHANGE_RATE)!, params: params)
        Alamofire.request((urlComponent.url?.absoluteString)!, method:.get, parameters:nil, encoding: JSONEncoding.default)
            .validate(statusCode: 200 ..< 300)
            .responseJSON{ response in
                switch response.result {
                case .success:
                    if let json = response.result.value as AnyObject? {
                        self.handleLatestExchangeRatesSuccessResponse(json: json)
                    }
                case .failure(_):
                    if (response.response?.statusCode) != nil {
                self.presenter?.currenciesPairFetchedSuccess(currenciesPairModelArray:Array<CurrencyPairModel>())
                    }
                }
            }
        }
        else {
            let aryCurrencyPair = fetchCurrencyPair()
            self.presenter?.currenciesPairFetchedSuccess(currenciesPairModelArray: aryCurrencyPair.sorted(by: { $0.pid > $1.pid }))
        }
    }
    
    internal func handleLatestExchangeRatesSuccessResponse(json:AnyObject) {
        let aryCurrencyPair = self.fetchCurrencyPair()
        var aryUpdatedCurrencyPair = Array<CurrencyPairModel>()
        let dicPairs = json as! Dictionary<String, AnyObject>
        for anItem in dicPairs {
            let aryObjects = aryCurrencyPair.filter{$0.exchangePair == anItem.key}
            if aryObjects.count > 0 {
                DispatchQueue.main.async {
                    do {
                    let realm = try! Realm()
                    try! realm.write {
                        let object = aryObjects.first
                        object?.currencyExchangeRate = String(format: "%@", anItem.value as! CVarArg)
                        aryUpdatedCurrencyPair.append(object!)
                        if aryUpdatedCurrencyPair.count == dicPairs.count {
                            self.presenter?.currenciesPairFetchedSuccess(currenciesPairModelArray: aryUpdatedCurrencyPair.sorted(by: { $0.pid > $1.pid }))
                        }
                    }
                } catch let error as NSError {
                    // handle error
                    print("error - \(error.localizedDescription)")
                }

                }
            }
        }
    }
    
    internal func setUrlPathComponent(url:URL,params: [(String, String)])-> URLComponents{
        let queryItems = params.map { URLQueryItem(name: $0.0, value: $0.1) }
        var urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: true)!
        urlComponents.queryItems =   queryItems
        return urlComponents
        }
    }
    


