//
//  LoginPresenter.swift
//  CurrenciesConverter
//
//  Created by Ashish Patel on 2019/07/13.
//  Copyright © 2019 Ashish Patel. All rights reserved.
//

import Foundation
import UIKit

class currenciesPairPresenter:ViewToPresenterProtocol {
    
    var view: PresenterToViewProtocol?
    
    var interactor: PresenterToInteractorProtocol?
    
    var router: PresenterToRouterProtocol?
    
    func startFetchingCurrenciesPair() {
        interactor?.getLatestExchangeRates()
    }
    
    func showSelectCurrencyController(navigationController: UINavigationController,currenciesPair:Array<CurrencyPairModel>) {
        router?.pushToSelectCurrencyScreen(navigationConroller:navigationController,arySelectedCurrenciesPair:currenciesPair)
    }
}

extension currenciesPairPresenter: InteractorToPresenterProtocol{    
    func currenciesPairFetchedSuccess(currenciesPairModelArray: Array<CurrencyPairModel>) {
        view?.showCurrenciesPair(currenciesPairArray: currenciesPairModelArray)
    }
    
}
