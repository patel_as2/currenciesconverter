//
//  selectCurrencyPresenterTests.swift
//  CurrenciesConverterTests
//
//  Created by Ashish Patel on 2019/07/16.
//  Copyright © 2019 Ashish Patel. All rights reserved.
//

import XCTest
import Cuckoo
@testable import CurrenciesConverter

class selectCurrencyPresenterTests: XCTestCase {

    private var currencyPresenter: selectCurrencyPresenter!
    
    let mockView = MockPresenterToViewSelectCurrencyProtocol()
    let mockInteractor = MockPresenterToInteractorSelectCurrencyProtocol()
    let mockRouter = MockPresenterToRouterSelectCurrencyProtocol()
    let mockInteractorPresenter = MockInteractorToPresenterSelectCurrencyProtocol()
    
    
    override func setUp() {
        currencyPresenter = selectCurrencyPresenter()
        currencyPresenter.router = mockRouter
        currencyPresenter.interactor = mockInteractor
        currencyPresenter.view = mockView
        
    }

    func testStartFetchingCurrenciesShouldCallFetchCurrencyListInInteractor() {
        
        stub(mockInteractor) { stub in
            when(stub).fetchCurrencyList().thenDoNothing()
        }
        
        currencyPresenter.startFetchingCurrencies()
        
        verify(mockInteractor).fetchCurrencyList()
    }
    
    func testAnimateSelectCurrencyController() {
        let vc = UIViewController()
        
        stub(mockRouter) { stub in
            when(stub).animateSelectCurrencyScreen(viewController: any()).thenDoNothing()
        }
        
        currencyPresenter.animateSelectCurrencyController(viewController: vc)

        verify(mockRouter).animateSelectCurrencyScreen(viewController: sameInstance(as: vc))
    }
    
    func testAddCurrencyPair() {
        let currencyModel = CurrencyPairModel()
        
        stub(mockInteractor) { stub in
            when(stub).addCurrencyPair(objCurrencyPair: any()).thenDoNothing()
        }

        currencyPresenter.addCurrencyPair(objCurrencyPair: currencyModel)

        verify(mockInteractor).addCurrencyPair(objCurrencyPair: sameInstance(as: currencyModel))
    }
    
    func testAddCurrencyPairSuccess() {
        stub(mockView) { stub in
            when(stub).onAddCurrencyResponse(result: any()).thenDoNothing()
        }
        
        currencyPresenter.addCurrencyPairSuccess(result: true)
        
        verify(mockView).onAddCurrencyResponse(result: true)
    }
    
    func testSelectCurrencyFetchSuccess() {
        let testArray = Array<SelectCurrencyModel>()
        
        stub(mockView) { stub in
            when(stub).onSelectCurrencyResponseSuccess(currencyModelArrayList: any()).thenDoNothing()
        }
        
        currencyPresenter.selectCurrencyFetchSuccess(currenciesList: testArray)
        
        verify(mockView).onSelectCurrencyResponseSuccess(currencyModelArrayList: any())
    }
    
    //test helpers
    
    private func sameInstance(as expected: UIViewController) -> ParameterMatcher<UIViewController> {
        return ParameterMatcher { actual in
                actual === expected
        }
    }
    
    private func sameInstance(as expected: CurrencyPairModel) -> ParameterMatcher<CurrencyPairModel> {
        return ParameterMatcher { actual in
            actual === expected
        }
    }

}
