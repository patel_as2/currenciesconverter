// MARK: - Mocks generated from file: CurrenciesConverter/app-modules/currenciesPair-module/interactor/currencyPairInteractor.swift at 2019-07-18 13:59:49 +0000

//
//  currencyPairInteractor.swift
//  CurrenciesConverter
//
//  Created by Ashish Patel on 2019/07/13.
//  Copyright © 2019 Ashish Patel. All rights reserved.
//

import Cuckoo
@testable import CurrenciesConverter

import Alamofire
import Foundation
import ObjectMapper
import RealmSwift


 class MockcurrencyPairInteractor: currencyPairInteractor, Cuckoo.ClassMock {
    
     typealias MocksType = currencyPairInteractor
    
     typealias Stubbing = __StubbingProxy_currencyPairInteractor
     typealias Verification = __VerificationProxy_currencyPairInteractor

     let cuckoo_manager = Cuckoo.MockManager.preconfiguredManager ?? Cuckoo.MockManager(hasParent: true)

    
    private var __defaultImplStub: currencyPairInteractor?

     func enableDefaultImplementation(_ stub: currencyPairInteractor) {
        __defaultImplStub = stub
        cuckoo_manager.enableDefaultStubImplementation()
    }
    

    
    
    
     override var presenter: InteractorToPresenterProtocol? {
        get {
            return cuckoo_manager.getter("presenter",
                superclassCall:
                    
                    super.presenter
                    ,
                defaultCall: __defaultImplStub!.presenter)
        }
        
        set {
            cuckoo_manager.setter("presenter",
                value: newValue,
                superclassCall:
                    
                    super.presenter = newValue
                    ,
                defaultCall: __defaultImplStub!.presenter = newValue)
        }
        
    }
    

    

    
    
    
     override func fetchCurrencyPair() -> Array<CurrencyPairModel> {
        
    return cuckoo_manager.call("fetchCurrencyPair() -> Array<CurrencyPairModel>",
            parameters: (),
            escapingParameters: (),
            superclassCall:
                
                super.fetchCurrencyPair()
                ,
            defaultCall: __defaultImplStub!.fetchCurrencyPair())
        
    }
    
    
    
     override func getLatestExchangeRatesAPIParms() -> [(String, String)] {
        
    return cuckoo_manager.call("getLatestExchangeRatesAPIParms() -> [(String, String)]",
            parameters: (),
            escapingParameters: (),
            superclassCall:
                
                super.getLatestExchangeRatesAPIParms()
                ,
            defaultCall: __defaultImplStub!.getLatestExchangeRatesAPIParms())
        
    }
    
    
    
     override func getLatestExchangeRates()  {
        
    return cuckoo_manager.call("getLatestExchangeRates()",
            parameters: (),
            escapingParameters: (),
            superclassCall:
                
                super.getLatestExchangeRates()
                ,
            defaultCall: __defaultImplStub!.getLatestExchangeRates())
        
    }
    
    
    
     override func handleLatestExchangeRatesSuccessResponse(json: AnyObject)  {
        
    return cuckoo_manager.call("handleLatestExchangeRatesSuccessResponse(json: AnyObject)",
            parameters: (json),
            escapingParameters: (json),
            superclassCall:
                
                super.handleLatestExchangeRatesSuccessResponse(json: json)
                ,
            defaultCall: __defaultImplStub!.handleLatestExchangeRatesSuccessResponse(json: json))
        
    }
    
    
    
     override func setUrlPathComponent(url: URL, params: [(String, String)]) -> URLComponents {
        
    return cuckoo_manager.call("setUrlPathComponent(url: URL, params: [(String, String)]) -> URLComponents",
            parameters: (url, params),
            escapingParameters: (url, params),
            superclassCall:
                
                super.setUrlPathComponent(url: url, params: params)
                ,
            defaultCall: __defaultImplStub!.setUrlPathComponent(url: url, params: params))
        
    }
    

	 struct __StubbingProxy_currencyPairInteractor: Cuckoo.StubbingProxy {
	    private let cuckoo_manager: Cuckoo.MockManager
	
	     init(manager: Cuckoo.MockManager) {
	        self.cuckoo_manager = manager
	    }
	    
	    
	    var presenter: Cuckoo.ClassToBeStubbedOptionalProperty<MockcurrencyPairInteractor, InteractorToPresenterProtocol> {
	        return .init(manager: cuckoo_manager, name: "presenter")
	    }
	    
	    
	    func fetchCurrencyPair() -> Cuckoo.ClassStubFunction<(), Array<CurrencyPairModel>> {
	        let matchers: [Cuckoo.ParameterMatcher<Void>] = []
	        return .init(stub: cuckoo_manager.createStub(for: MockcurrencyPairInteractor.self, method: "fetchCurrencyPair() -> Array<CurrencyPairModel>", parameterMatchers: matchers))
	    }
	    
	    func getLatestExchangeRatesAPIParms() -> Cuckoo.ClassStubFunction<(), [(String, String)]> {
	        let matchers: [Cuckoo.ParameterMatcher<Void>] = []
	        return .init(stub: cuckoo_manager.createStub(for: MockcurrencyPairInteractor.self, method: "getLatestExchangeRatesAPIParms() -> [(String, String)]", parameterMatchers: matchers))
	    }
	    
	    func getLatestExchangeRates() -> Cuckoo.ClassStubNoReturnFunction<()> {
	        let matchers: [Cuckoo.ParameterMatcher<Void>] = []
	        return .init(stub: cuckoo_manager.createStub(for: MockcurrencyPairInteractor.self, method: "getLatestExchangeRates()", parameterMatchers: matchers))
	    }
	    
	    func handleLatestExchangeRatesSuccessResponse<M1: Cuckoo.Matchable>(json: M1) -> Cuckoo.ClassStubNoReturnFunction<(AnyObject)> where M1.MatchedType == AnyObject {
	        let matchers: [Cuckoo.ParameterMatcher<(AnyObject)>] = [wrap(matchable: json) { $0 }]
	        return .init(stub: cuckoo_manager.createStub(for: MockcurrencyPairInteractor.self, method: "handleLatestExchangeRatesSuccessResponse(json: AnyObject)", parameterMatchers: matchers))
	    }
	    
	    func setUrlPathComponent<M1: Cuckoo.Matchable, M2: Cuckoo.Matchable>(url: M1, params: M2) -> Cuckoo.ClassStubFunction<(URL, [(String, String)]), URLComponents> where M1.MatchedType == URL, M2.MatchedType == [(String, String)] {
	        let matchers: [Cuckoo.ParameterMatcher<(URL, [(String, String)])>] = [wrap(matchable: url) { $0.0 }, wrap(matchable: params) { $0.1 }]
	        return .init(stub: cuckoo_manager.createStub(for: MockcurrencyPairInteractor.self, method: "setUrlPathComponent(url: URL, params: [(String, String)]) -> URLComponents", parameterMatchers: matchers))
	    }
	    
	}

	 struct __VerificationProxy_currencyPairInteractor: Cuckoo.VerificationProxy {
	    private let cuckoo_manager: Cuckoo.MockManager
	    private let callMatcher: Cuckoo.CallMatcher
	    private let sourceLocation: Cuckoo.SourceLocation
	
	     init(manager: Cuckoo.MockManager, callMatcher: Cuckoo.CallMatcher, sourceLocation: Cuckoo.SourceLocation) {
	        self.cuckoo_manager = manager
	        self.callMatcher = callMatcher
	        self.sourceLocation = sourceLocation
	    }
	
	    
	    
	    var presenter: Cuckoo.VerifyOptionalProperty<InteractorToPresenterProtocol> {
	        return .init(manager: cuckoo_manager, name: "presenter", callMatcher: callMatcher, sourceLocation: sourceLocation)
	    }
	    
	
	    
	    @discardableResult
	    func fetchCurrencyPair() -> Cuckoo.__DoNotUse<(), Array<CurrencyPairModel>> {
	        let matchers: [Cuckoo.ParameterMatcher<Void>] = []
	        return cuckoo_manager.verify("fetchCurrencyPair() -> Array<CurrencyPairModel>", callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
	    }
	    
	    @discardableResult
	    func getLatestExchangeRatesAPIParms() -> Cuckoo.__DoNotUse<(), [(String, String)]> {
	        let matchers: [Cuckoo.ParameterMatcher<Void>] = []
	        return cuckoo_manager.verify("getLatestExchangeRatesAPIParms() -> [(String, String)]", callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
	    }
	    
	    @discardableResult
	    func getLatestExchangeRates() -> Cuckoo.__DoNotUse<(), Void> {
	        let matchers: [Cuckoo.ParameterMatcher<Void>] = []
	        return cuckoo_manager.verify("getLatestExchangeRates()", callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
	    }
	    
	    @discardableResult
	    func handleLatestExchangeRatesSuccessResponse<M1: Cuckoo.Matchable>(json: M1) -> Cuckoo.__DoNotUse<(AnyObject), Void> where M1.MatchedType == AnyObject {
	        let matchers: [Cuckoo.ParameterMatcher<(AnyObject)>] = [wrap(matchable: json) { $0 }]
	        return cuckoo_manager.verify("handleLatestExchangeRatesSuccessResponse(json: AnyObject)", callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
	    }
	    
	    @discardableResult
	    func setUrlPathComponent<M1: Cuckoo.Matchable, M2: Cuckoo.Matchable>(url: M1, params: M2) -> Cuckoo.__DoNotUse<(URL, [(String, String)]), URLComponents> where M1.MatchedType == URL, M2.MatchedType == [(String, String)] {
	        let matchers: [Cuckoo.ParameterMatcher<(URL, [(String, String)])>] = [wrap(matchable: url) { $0.0 }, wrap(matchable: params) { $0.1 }]
	        return cuckoo_manager.verify("setUrlPathComponent(url: URL, params: [(String, String)]) -> URLComponents", callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
	    }
	    
	}
}

 class currencyPairInteractorStub: currencyPairInteractor {
    
    
     override var presenter: InteractorToPresenterProtocol? {
        get {
            return DefaultValueRegistry.defaultValue(for: (InteractorToPresenterProtocol?).self)
        }
        
        set { }
        
    }
    

    

    
     override func fetchCurrencyPair() -> Array<CurrencyPairModel>  {
        return DefaultValueRegistry.defaultValue(for: (Array<CurrencyPairModel>).self)
    }
    
     override func getLatestExchangeRatesAPIParms() -> [(String, String)]  {
        return DefaultValueRegistry.defaultValue(for: ([(String, String)]).self)
    }
    
     override func getLatestExchangeRates()   {
        return DefaultValueRegistry.defaultValue(for: (Void).self)
    }
    
     override func handleLatestExchangeRatesSuccessResponse(json: AnyObject)   {
        return DefaultValueRegistry.defaultValue(for: (Void).self)
    }
    
     override func setUrlPathComponent(url: URL, params: [(String, String)]) -> URLComponents  {
        return DefaultValueRegistry.defaultValue(for: (URLComponents).self)
    }
    
}


// MARK: - Mocks generated from file: CurrenciesConverter/app-modules/currenciesPair-module/protocols/currenciesPairProtocols.swift at 2019-07-18 13:59:49 +0000

//
//  currenciesPairProtocols.swift
//  CurrenciesConverter
//
//  Created by Ashish Patel on 2019/07/13.
//  Copyright © 2019 Ashish Patel. All rights reserved.
//

import Cuckoo
@testable import CurrenciesConverter

import Foundation
import UIKit


 class MockViewToPresenterProtocol: ViewToPresenterProtocol, Cuckoo.ProtocolMock {
    
     typealias MocksType = ViewToPresenterProtocol
    
     typealias Stubbing = __StubbingProxy_ViewToPresenterProtocol
     typealias Verification = __VerificationProxy_ViewToPresenterProtocol

     let cuckoo_manager = Cuckoo.MockManager.preconfiguredManager ?? Cuckoo.MockManager(hasParent: false)

    
    private var __defaultImplStub: ViewToPresenterProtocol?

     func enableDefaultImplementation(_ stub: ViewToPresenterProtocol) {
        __defaultImplStub = stub
        cuckoo_manager.enableDefaultStubImplementation()
    }
    

    
    
    
     var view: PresenterToViewProtocol? {
        get {
            return cuckoo_manager.getter("view",
                superclassCall:
                    
                    Cuckoo.MockManager.crashOnProtocolSuperclassCall()
                    ,
                defaultCall: __defaultImplStub!.view)
        }
        
        set {
            cuckoo_manager.setter("view",
                value: newValue,
                superclassCall:
                    
                    Cuckoo.MockManager.crashOnProtocolSuperclassCall()
                    ,
                defaultCall: __defaultImplStub!.view = newValue)
        }
        
    }
    
    
    
     var interactor: PresenterToInteractorProtocol? {
        get {
            return cuckoo_manager.getter("interactor",
                superclassCall:
                    
                    Cuckoo.MockManager.crashOnProtocolSuperclassCall()
                    ,
                defaultCall: __defaultImplStub!.interactor)
        }
        
        set {
            cuckoo_manager.setter("interactor",
                value: newValue,
                superclassCall:
                    
                    Cuckoo.MockManager.crashOnProtocolSuperclassCall()
                    ,
                defaultCall: __defaultImplStub!.interactor = newValue)
        }
        
    }
    
    
    
     var router: PresenterToRouterProtocol? {
        get {
            return cuckoo_manager.getter("router",
                superclassCall:
                    
                    Cuckoo.MockManager.crashOnProtocolSuperclassCall()
                    ,
                defaultCall: __defaultImplStub!.router)
        }
        
        set {
            cuckoo_manager.setter("router",
                value: newValue,
                superclassCall:
                    
                    Cuckoo.MockManager.crashOnProtocolSuperclassCall()
                    ,
                defaultCall: __defaultImplStub!.router = newValue)
        }
        
    }
    

    

    
    
    
     func startFetchingCurrenciesPair()  {
        
    return cuckoo_manager.call("startFetchingCurrenciesPair()",
            parameters: (),
            escapingParameters: (),
            superclassCall:
                
                Cuckoo.MockManager.crashOnProtocolSuperclassCall()
                ,
            defaultCall: __defaultImplStub!.startFetchingCurrenciesPair())
        
    }
    
    
    
     func showSelectCurrencyController(navigationController: UINavigationController, currenciesPair: Array<CurrencyPairModel>)  {
        
    return cuckoo_manager.call("showSelectCurrencyController(navigationController: UINavigationController, currenciesPair: Array<CurrencyPairModel>)",
            parameters: (navigationController, currenciesPair),
            escapingParameters: (navigationController, currenciesPair),
            superclassCall:
                
                Cuckoo.MockManager.crashOnProtocolSuperclassCall()
                ,
            defaultCall: __defaultImplStub!.showSelectCurrencyController(navigationController: navigationController, currenciesPair: currenciesPair))
        
    }
    

	 struct __StubbingProxy_ViewToPresenterProtocol: Cuckoo.StubbingProxy {
	    private let cuckoo_manager: Cuckoo.MockManager
	
	     init(manager: Cuckoo.MockManager) {
	        self.cuckoo_manager = manager
	    }
	    
	    
	    var view: Cuckoo.ProtocolToBeStubbedOptionalProperty<MockViewToPresenterProtocol, PresenterToViewProtocol> {
	        return .init(manager: cuckoo_manager, name: "view")
	    }
	    
	    
	    var interactor: Cuckoo.ProtocolToBeStubbedOptionalProperty<MockViewToPresenterProtocol, PresenterToInteractorProtocol> {
	        return .init(manager: cuckoo_manager, name: "interactor")
	    }
	    
	    
	    var router: Cuckoo.ProtocolToBeStubbedOptionalProperty<MockViewToPresenterProtocol, PresenterToRouterProtocol> {
	        return .init(manager: cuckoo_manager, name: "router")
	    }
	    
	    
	    func startFetchingCurrenciesPair() -> Cuckoo.ProtocolStubNoReturnFunction<()> {
	        let matchers: [Cuckoo.ParameterMatcher<Void>] = []
	        return .init(stub: cuckoo_manager.createStub(for: MockViewToPresenterProtocol.self, method: "startFetchingCurrenciesPair()", parameterMatchers: matchers))
	    }
	    
	    func showSelectCurrencyController<M1: Cuckoo.Matchable, M2: Cuckoo.Matchable>(navigationController: M1, currenciesPair: M2) -> Cuckoo.ProtocolStubNoReturnFunction<(UINavigationController, Array<CurrencyPairModel>)> where M1.MatchedType == UINavigationController, M2.MatchedType == Array<CurrencyPairModel> {
	        let matchers: [Cuckoo.ParameterMatcher<(UINavigationController, Array<CurrencyPairModel>)>] = [wrap(matchable: navigationController) { $0.0 }, wrap(matchable: currenciesPair) { $0.1 }]
	        return .init(stub: cuckoo_manager.createStub(for: MockViewToPresenterProtocol.self, method: "showSelectCurrencyController(navigationController: UINavigationController, currenciesPair: Array<CurrencyPairModel>)", parameterMatchers: matchers))
	    }
	    
	}

	 struct __VerificationProxy_ViewToPresenterProtocol: Cuckoo.VerificationProxy {
	    private let cuckoo_manager: Cuckoo.MockManager
	    private let callMatcher: Cuckoo.CallMatcher
	    private let sourceLocation: Cuckoo.SourceLocation
	
	     init(manager: Cuckoo.MockManager, callMatcher: Cuckoo.CallMatcher, sourceLocation: Cuckoo.SourceLocation) {
	        self.cuckoo_manager = manager
	        self.callMatcher = callMatcher
	        self.sourceLocation = sourceLocation
	    }
	
	    
	    
	    var view: Cuckoo.VerifyOptionalProperty<PresenterToViewProtocol> {
	        return .init(manager: cuckoo_manager, name: "view", callMatcher: callMatcher, sourceLocation: sourceLocation)
	    }
	    
	    
	    var interactor: Cuckoo.VerifyOptionalProperty<PresenterToInteractorProtocol> {
	        return .init(manager: cuckoo_manager, name: "interactor", callMatcher: callMatcher, sourceLocation: sourceLocation)
	    }
	    
	    
	    var router: Cuckoo.VerifyOptionalProperty<PresenterToRouterProtocol> {
	        return .init(manager: cuckoo_manager, name: "router", callMatcher: callMatcher, sourceLocation: sourceLocation)
	    }
	    
	
	    
	    @discardableResult
	    func startFetchingCurrenciesPair() -> Cuckoo.__DoNotUse<(), Void> {
	        let matchers: [Cuckoo.ParameterMatcher<Void>] = []
	        return cuckoo_manager.verify("startFetchingCurrenciesPair()", callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
	    }
	    
	    @discardableResult
	    func showSelectCurrencyController<M1: Cuckoo.Matchable, M2: Cuckoo.Matchable>(navigationController: M1, currenciesPair: M2) -> Cuckoo.__DoNotUse<(UINavigationController, Array<CurrencyPairModel>), Void> where M1.MatchedType == UINavigationController, M2.MatchedType == Array<CurrencyPairModel> {
	        let matchers: [Cuckoo.ParameterMatcher<(UINavigationController, Array<CurrencyPairModel>)>] = [wrap(matchable: navigationController) { $0.0 }, wrap(matchable: currenciesPair) { $0.1 }]
	        return cuckoo_manager.verify("showSelectCurrencyController(navigationController: UINavigationController, currenciesPair: Array<CurrencyPairModel>)", callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
	    }
	    
	}
}

 class ViewToPresenterProtocolStub: ViewToPresenterProtocol {
    
    
     var view: PresenterToViewProtocol? {
        get {
            return DefaultValueRegistry.defaultValue(for: (PresenterToViewProtocol?).self)
        }
        
        set { }
        
    }
    
    
     var interactor: PresenterToInteractorProtocol? {
        get {
            return DefaultValueRegistry.defaultValue(for: (PresenterToInteractorProtocol?).self)
        }
        
        set { }
        
    }
    
    
     var router: PresenterToRouterProtocol? {
        get {
            return DefaultValueRegistry.defaultValue(for: (PresenterToRouterProtocol?).self)
        }
        
        set { }
        
    }
    

    

    
     func startFetchingCurrenciesPair()   {
        return DefaultValueRegistry.defaultValue(for: (Void).self)
    }
    
     func showSelectCurrencyController(navigationController: UINavigationController, currenciesPair: Array<CurrencyPairModel>)   {
        return DefaultValueRegistry.defaultValue(for: (Void).self)
    }
    
}



 class MockPresenterToViewProtocol: PresenterToViewProtocol, Cuckoo.ProtocolMock {
    
     typealias MocksType = PresenterToViewProtocol
    
     typealias Stubbing = __StubbingProxy_PresenterToViewProtocol
     typealias Verification = __VerificationProxy_PresenterToViewProtocol

     let cuckoo_manager = Cuckoo.MockManager.preconfiguredManager ?? Cuckoo.MockManager(hasParent: false)

    
    private var __defaultImplStub: PresenterToViewProtocol?

     func enableDefaultImplementation(_ stub: PresenterToViewProtocol) {
        __defaultImplStub = stub
        cuckoo_manager.enableDefaultStubImplementation()
    }
    

    

    

    
    
    
     func showCurrenciesPair(currenciesPairArray: Array<CurrencyPairModel>)  {
        
    return cuckoo_manager.call("showCurrenciesPair(currenciesPairArray: Array<CurrencyPairModel>)",
            parameters: (currenciesPairArray),
            escapingParameters: (currenciesPairArray),
            superclassCall:
                
                Cuckoo.MockManager.crashOnProtocolSuperclassCall()
                ,
            defaultCall: __defaultImplStub!.showCurrenciesPair(currenciesPairArray: currenciesPairArray))
        
    }
    

	 struct __StubbingProxy_PresenterToViewProtocol: Cuckoo.StubbingProxy {
	    private let cuckoo_manager: Cuckoo.MockManager
	
	     init(manager: Cuckoo.MockManager) {
	        self.cuckoo_manager = manager
	    }
	    
	    
	    func showCurrenciesPair<M1: Cuckoo.Matchable>(currenciesPairArray: M1) -> Cuckoo.ProtocolStubNoReturnFunction<(Array<CurrencyPairModel>)> where M1.MatchedType == Array<CurrencyPairModel> {
	        let matchers: [Cuckoo.ParameterMatcher<(Array<CurrencyPairModel>)>] = [wrap(matchable: currenciesPairArray) { $0 }]
	        return .init(stub: cuckoo_manager.createStub(for: MockPresenterToViewProtocol.self, method: "showCurrenciesPair(currenciesPairArray: Array<CurrencyPairModel>)", parameterMatchers: matchers))
	    }
	    
	}

	 struct __VerificationProxy_PresenterToViewProtocol: Cuckoo.VerificationProxy {
	    private let cuckoo_manager: Cuckoo.MockManager
	    private let callMatcher: Cuckoo.CallMatcher
	    private let sourceLocation: Cuckoo.SourceLocation
	
	     init(manager: Cuckoo.MockManager, callMatcher: Cuckoo.CallMatcher, sourceLocation: Cuckoo.SourceLocation) {
	        self.cuckoo_manager = manager
	        self.callMatcher = callMatcher
	        self.sourceLocation = sourceLocation
	    }
	
	    
	
	    
	    @discardableResult
	    func showCurrenciesPair<M1: Cuckoo.Matchable>(currenciesPairArray: M1) -> Cuckoo.__DoNotUse<(Array<CurrencyPairModel>), Void> where M1.MatchedType == Array<CurrencyPairModel> {
	        let matchers: [Cuckoo.ParameterMatcher<(Array<CurrencyPairModel>)>] = [wrap(matchable: currenciesPairArray) { $0 }]
	        return cuckoo_manager.verify("showCurrenciesPair(currenciesPairArray: Array<CurrencyPairModel>)", callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
	    }
	    
	}
}

 class PresenterToViewProtocolStub: PresenterToViewProtocol {
    

    

    
     func showCurrenciesPair(currenciesPairArray: Array<CurrencyPairModel>)   {
        return DefaultValueRegistry.defaultValue(for: (Void).self)
    }
    
}



 class MockPresenterToRouterProtocol: PresenterToRouterProtocol, Cuckoo.ProtocolMock {
    
     typealias MocksType = PresenterToRouterProtocol
    
     typealias Stubbing = __StubbingProxy_PresenterToRouterProtocol
     typealias Verification = __VerificationProxy_PresenterToRouterProtocol

     let cuckoo_manager = Cuckoo.MockManager.preconfiguredManager ?? Cuckoo.MockManager(hasParent: false)

    
    private var __defaultImplStub: PresenterToRouterProtocol?

     func enableDefaultImplementation(_ stub: PresenterToRouterProtocol) {
        __defaultImplStub = stub
        cuckoo_manager.enableDefaultStubImplementation()
    }
    

    

    

    
    
    
     func createModule() -> currenciesPairVC {
        
    return cuckoo_manager.call("createModule() -> currenciesPairVC",
            parameters: (),
            escapingParameters: (),
            superclassCall:
                
                Cuckoo.MockManager.crashOnProtocolSuperclassCall()
                ,
            defaultCall: __defaultImplStub!.createModule())
        
    }
    
    
    
     func pushToSelectCurrencyScreen(navigationConroller: UINavigationController, arySelectedCurrenciesPair: Array<CurrencyPairModel>)  {
        
    return cuckoo_manager.call("pushToSelectCurrencyScreen(navigationConroller: UINavigationController, arySelectedCurrenciesPair: Array<CurrencyPairModel>)",
            parameters: (navigationConroller, arySelectedCurrenciesPair),
            escapingParameters: (navigationConroller, arySelectedCurrenciesPair),
            superclassCall:
                
                Cuckoo.MockManager.crashOnProtocolSuperclassCall()
                ,
            defaultCall: __defaultImplStub!.pushToSelectCurrencyScreen(navigationConroller: navigationConroller, arySelectedCurrenciesPair: arySelectedCurrenciesPair))
        
    }
    

	 struct __StubbingProxy_PresenterToRouterProtocol: Cuckoo.StubbingProxy {
	    private let cuckoo_manager: Cuckoo.MockManager
	
	     init(manager: Cuckoo.MockManager) {
	        self.cuckoo_manager = manager
	    }
	    
	    
	    func createModule() -> Cuckoo.ProtocolStubFunction<(), currenciesPairVC> {
	        let matchers: [Cuckoo.ParameterMatcher<Void>] = []
	        return .init(stub: cuckoo_manager.createStub(for: MockPresenterToRouterProtocol.self, method: "createModule() -> currenciesPairVC", parameterMatchers: matchers))
	    }
	    
	    func pushToSelectCurrencyScreen<M1: Cuckoo.Matchable, M2: Cuckoo.Matchable>(navigationConroller: M1, arySelectedCurrenciesPair: M2) -> Cuckoo.ProtocolStubNoReturnFunction<(UINavigationController, Array<CurrencyPairModel>)> where M1.MatchedType == UINavigationController, M2.MatchedType == Array<CurrencyPairModel> {
	        let matchers: [Cuckoo.ParameterMatcher<(UINavigationController, Array<CurrencyPairModel>)>] = [wrap(matchable: navigationConroller) { $0.0 }, wrap(matchable: arySelectedCurrenciesPair) { $0.1 }]
	        return .init(stub: cuckoo_manager.createStub(for: MockPresenterToRouterProtocol.self, method: "pushToSelectCurrencyScreen(navigationConroller: UINavigationController, arySelectedCurrenciesPair: Array<CurrencyPairModel>)", parameterMatchers: matchers))
	    }
	    
	}

	 struct __VerificationProxy_PresenterToRouterProtocol: Cuckoo.VerificationProxy {
	    private let cuckoo_manager: Cuckoo.MockManager
	    private let callMatcher: Cuckoo.CallMatcher
	    private let sourceLocation: Cuckoo.SourceLocation
	
	     init(manager: Cuckoo.MockManager, callMatcher: Cuckoo.CallMatcher, sourceLocation: Cuckoo.SourceLocation) {
	        self.cuckoo_manager = manager
	        self.callMatcher = callMatcher
	        self.sourceLocation = sourceLocation
	    }
	
	    
	
	    
	    @discardableResult
	    func createModule() -> Cuckoo.__DoNotUse<(), currenciesPairVC> {
	        let matchers: [Cuckoo.ParameterMatcher<Void>] = []
	        return cuckoo_manager.verify("createModule() -> currenciesPairVC", callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
	    }
	    
	    @discardableResult
	    func pushToSelectCurrencyScreen<M1: Cuckoo.Matchable, M2: Cuckoo.Matchable>(navigationConroller: M1, arySelectedCurrenciesPair: M2) -> Cuckoo.__DoNotUse<(UINavigationController, Array<CurrencyPairModel>), Void> where M1.MatchedType == UINavigationController, M2.MatchedType == Array<CurrencyPairModel> {
	        let matchers: [Cuckoo.ParameterMatcher<(UINavigationController, Array<CurrencyPairModel>)>] = [wrap(matchable: navigationConroller) { $0.0 }, wrap(matchable: arySelectedCurrenciesPair) { $0.1 }]
	        return cuckoo_manager.verify("pushToSelectCurrencyScreen(navigationConroller: UINavigationController, arySelectedCurrenciesPair: Array<CurrencyPairModel>)", callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
	    }
	    
	}
}

 class PresenterToRouterProtocolStub: PresenterToRouterProtocol {
    

    

    
     func createModule() -> currenciesPairVC  {
        return DefaultValueRegistry.defaultValue(for: (currenciesPairVC).self)
    }
    
     func pushToSelectCurrencyScreen(navigationConroller: UINavigationController, arySelectedCurrenciesPair: Array<CurrencyPairModel>)   {
        return DefaultValueRegistry.defaultValue(for: (Void).self)
    }
    
}



 class MockPresenterToInteractorProtocol: PresenterToInteractorProtocol, Cuckoo.ProtocolMock {
    
     typealias MocksType = PresenterToInteractorProtocol
    
     typealias Stubbing = __StubbingProxy_PresenterToInteractorProtocol
     typealias Verification = __VerificationProxy_PresenterToInteractorProtocol

     let cuckoo_manager = Cuckoo.MockManager.preconfiguredManager ?? Cuckoo.MockManager(hasParent: false)

    
    private var __defaultImplStub: PresenterToInteractorProtocol?

     func enableDefaultImplementation(_ stub: PresenterToInteractorProtocol) {
        __defaultImplStub = stub
        cuckoo_manager.enableDefaultStubImplementation()
    }
    

    
    
    
     var presenter: InteractorToPresenterProtocol? {
        get {
            return cuckoo_manager.getter("presenter",
                superclassCall:
                    
                    Cuckoo.MockManager.crashOnProtocolSuperclassCall()
                    ,
                defaultCall: __defaultImplStub!.presenter)
        }
        
        set {
            cuckoo_manager.setter("presenter",
                value: newValue,
                superclassCall:
                    
                    Cuckoo.MockManager.crashOnProtocolSuperclassCall()
                    ,
                defaultCall: __defaultImplStub!.presenter = newValue)
        }
        
    }
    

    

    
    
    
     func getLatestExchangeRates()  {
        
    return cuckoo_manager.call("getLatestExchangeRates()",
            parameters: (),
            escapingParameters: (),
            superclassCall:
                
                Cuckoo.MockManager.crashOnProtocolSuperclassCall()
                ,
            defaultCall: __defaultImplStub!.getLatestExchangeRates())
        
    }
    

	 struct __StubbingProxy_PresenterToInteractorProtocol: Cuckoo.StubbingProxy {
	    private let cuckoo_manager: Cuckoo.MockManager
	
	     init(manager: Cuckoo.MockManager) {
	        self.cuckoo_manager = manager
	    }
	    
	    
	    var presenter: Cuckoo.ProtocolToBeStubbedOptionalProperty<MockPresenterToInteractorProtocol, InteractorToPresenterProtocol> {
	        return .init(manager: cuckoo_manager, name: "presenter")
	    }
	    
	    
	    func getLatestExchangeRates() -> Cuckoo.ProtocolStubNoReturnFunction<()> {
	        let matchers: [Cuckoo.ParameterMatcher<Void>] = []
	        return .init(stub: cuckoo_manager.createStub(for: MockPresenterToInteractorProtocol.self, method: "getLatestExchangeRates()", parameterMatchers: matchers))
	    }
	    
	}

	 struct __VerificationProxy_PresenterToInteractorProtocol: Cuckoo.VerificationProxy {
	    private let cuckoo_manager: Cuckoo.MockManager
	    private let callMatcher: Cuckoo.CallMatcher
	    private let sourceLocation: Cuckoo.SourceLocation
	
	     init(manager: Cuckoo.MockManager, callMatcher: Cuckoo.CallMatcher, sourceLocation: Cuckoo.SourceLocation) {
	        self.cuckoo_manager = manager
	        self.callMatcher = callMatcher
	        self.sourceLocation = sourceLocation
	    }
	
	    
	    
	    var presenter: Cuckoo.VerifyOptionalProperty<InteractorToPresenterProtocol> {
	        return .init(manager: cuckoo_manager, name: "presenter", callMatcher: callMatcher, sourceLocation: sourceLocation)
	    }
	    
	
	    
	    @discardableResult
	    func getLatestExchangeRates() -> Cuckoo.__DoNotUse<(), Void> {
	        let matchers: [Cuckoo.ParameterMatcher<Void>] = []
	        return cuckoo_manager.verify("getLatestExchangeRates()", callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
	    }
	    
	}
}

 class PresenterToInteractorProtocolStub: PresenterToInteractorProtocol {
    
    
     var presenter: InteractorToPresenterProtocol? {
        get {
            return DefaultValueRegistry.defaultValue(for: (InteractorToPresenterProtocol?).self)
        }
        
        set { }
        
    }
    

    

    
     func getLatestExchangeRates()   {
        return DefaultValueRegistry.defaultValue(for: (Void).self)
    }
    
}



 class MockInteractorToPresenterProtocol: InteractorToPresenterProtocol, Cuckoo.ProtocolMock {
    
     typealias MocksType = InteractorToPresenterProtocol
    
     typealias Stubbing = __StubbingProxy_InteractorToPresenterProtocol
     typealias Verification = __VerificationProxy_InteractorToPresenterProtocol

     let cuckoo_manager = Cuckoo.MockManager.preconfiguredManager ?? Cuckoo.MockManager(hasParent: false)

    
    private var __defaultImplStub: InteractorToPresenterProtocol?

     func enableDefaultImplementation(_ stub: InteractorToPresenterProtocol) {
        __defaultImplStub = stub
        cuckoo_manager.enableDefaultStubImplementation()
    }
    

    

    

    
    
    
     func currenciesPairFetchedSuccess(currenciesPairModelArray: Array<CurrencyPairModel>)  {
        
    return cuckoo_manager.call("currenciesPairFetchedSuccess(currenciesPairModelArray: Array<CurrencyPairModel>)",
            parameters: (currenciesPairModelArray),
            escapingParameters: (currenciesPairModelArray),
            superclassCall:
                
                Cuckoo.MockManager.crashOnProtocolSuperclassCall()
                ,
            defaultCall: __defaultImplStub!.currenciesPairFetchedSuccess(currenciesPairModelArray: currenciesPairModelArray))
        
    }
    

	 struct __StubbingProxy_InteractorToPresenterProtocol: Cuckoo.StubbingProxy {
	    private let cuckoo_manager: Cuckoo.MockManager
	
	     init(manager: Cuckoo.MockManager) {
	        self.cuckoo_manager = manager
	    }
	    
	    
	    func currenciesPairFetchedSuccess<M1: Cuckoo.Matchable>(currenciesPairModelArray: M1) -> Cuckoo.ProtocolStubNoReturnFunction<(Array<CurrencyPairModel>)> where M1.MatchedType == Array<CurrencyPairModel> {
	        let matchers: [Cuckoo.ParameterMatcher<(Array<CurrencyPairModel>)>] = [wrap(matchable: currenciesPairModelArray) { $0 }]
	        return .init(stub: cuckoo_manager.createStub(for: MockInteractorToPresenterProtocol.self, method: "currenciesPairFetchedSuccess(currenciesPairModelArray: Array<CurrencyPairModel>)", parameterMatchers: matchers))
	    }
	    
	}

	 struct __VerificationProxy_InteractorToPresenterProtocol: Cuckoo.VerificationProxy {
	    private let cuckoo_manager: Cuckoo.MockManager
	    private let callMatcher: Cuckoo.CallMatcher
	    private let sourceLocation: Cuckoo.SourceLocation
	
	     init(manager: Cuckoo.MockManager, callMatcher: Cuckoo.CallMatcher, sourceLocation: Cuckoo.SourceLocation) {
	        self.cuckoo_manager = manager
	        self.callMatcher = callMatcher
	        self.sourceLocation = sourceLocation
	    }
	
	    
	
	    
	    @discardableResult
	    func currenciesPairFetchedSuccess<M1: Cuckoo.Matchable>(currenciesPairModelArray: M1) -> Cuckoo.__DoNotUse<(Array<CurrencyPairModel>), Void> where M1.MatchedType == Array<CurrencyPairModel> {
	        let matchers: [Cuckoo.ParameterMatcher<(Array<CurrencyPairModel>)>] = [wrap(matchable: currenciesPairModelArray) { $0 }]
	        return cuckoo_manager.verify("currenciesPairFetchedSuccess(currenciesPairModelArray: Array<CurrencyPairModel>)", callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
	    }
	    
	}
}

 class InteractorToPresenterProtocolStub: InteractorToPresenterProtocol {
    

    

    
     func currenciesPairFetchedSuccess(currenciesPairModelArray: Array<CurrencyPairModel>)   {
        return DefaultValueRegistry.defaultValue(for: (Void).self)
    }
    
}


// MARK: - Mocks generated from file: CurrenciesConverter/app-modules/selectCurrency-module/interactor/selectCurrencyInteractor.swift at 2019-07-18 13:59:49 +0000

//
//  selectCurrencyInteractor.swift
//  CurrenciesConverter
//
//  Created by Ashish Patel on 2019/07/13.
//  Copyright © 2019 Ashish Patel. All rights reserved.
//

import Cuckoo
@testable import CurrenciesConverter

import Alamofire
import Foundation
import ObjectMapper
import RealmSwift


 class MockselectCurrencyInteractor: selectCurrencyInteractor, Cuckoo.ClassMock {
    
     typealias MocksType = selectCurrencyInteractor
    
     typealias Stubbing = __StubbingProxy_selectCurrencyInteractor
     typealias Verification = __VerificationProxy_selectCurrencyInteractor

     let cuckoo_manager = Cuckoo.MockManager.preconfiguredManager ?? Cuckoo.MockManager(hasParent: true)

    
    private var __defaultImplStub: selectCurrencyInteractor?

     func enableDefaultImplementation(_ stub: selectCurrencyInteractor) {
        __defaultImplStub = stub
        cuckoo_manager.enableDefaultStubImplementation()
    }
    

    
    
    
     override var presenter: InteractorToPresenterSelectCurrencyProtocol? {
        get {
            return cuckoo_manager.getter("presenter",
                superclassCall:
                    
                    super.presenter
                    ,
                defaultCall: __defaultImplStub!.presenter)
        }
        
        set {
            cuckoo_manager.setter("presenter",
                value: newValue,
                superclassCall:
                    
                    super.presenter = newValue
                    ,
                defaultCall: __defaultImplStub!.presenter = newValue)
        }
        
    }
    

    

    
    
    
     override func fetchCurrencyList()  {
        
    return cuckoo_manager.call("fetchCurrencyList()",
            parameters: (),
            escapingParameters: (),
            superclassCall:
                
                super.fetchCurrencyList()
                ,
            defaultCall: __defaultImplStub!.fetchCurrencyList())
        
    }
    
    
    
     override func addCurrencyPair(objCurrencyPair: CurrencyPairModel)  {
        
    return cuckoo_manager.call("addCurrencyPair(objCurrencyPair: CurrencyPairModel)",
            parameters: (objCurrencyPair),
            escapingParameters: (objCurrencyPair),
            superclassCall:
                
                super.addCurrencyPair(objCurrencyPair: objCurrencyPair)
                ,
            defaultCall: __defaultImplStub!.addCurrencyPair(objCurrencyPair: objCurrencyPair))
        
    }
    
    
    
     override func incrementID() -> Int {
        
    return cuckoo_manager.call("incrementID() -> Int",
            parameters: (),
            escapingParameters: (),
            superclassCall:
                
                super.incrementID()
                ,
            defaultCall: __defaultImplStub!.incrementID())
        
    }
    

	 struct __StubbingProxy_selectCurrencyInteractor: Cuckoo.StubbingProxy {
	    private let cuckoo_manager: Cuckoo.MockManager
	
	     init(manager: Cuckoo.MockManager) {
	        self.cuckoo_manager = manager
	    }
	    
	    
	    var presenter: Cuckoo.ClassToBeStubbedOptionalProperty<MockselectCurrencyInteractor, InteractorToPresenterSelectCurrencyProtocol> {
	        return .init(manager: cuckoo_manager, name: "presenter")
	    }
	    
	    
	    func fetchCurrencyList() -> Cuckoo.ClassStubNoReturnFunction<()> {
	        let matchers: [Cuckoo.ParameterMatcher<Void>] = []
	        return .init(stub: cuckoo_manager.createStub(for: MockselectCurrencyInteractor.self, method: "fetchCurrencyList()", parameterMatchers: matchers))
	    }
	    
	    func addCurrencyPair<M1: Cuckoo.Matchable>(objCurrencyPair: M1) -> Cuckoo.ClassStubNoReturnFunction<(CurrencyPairModel)> where M1.MatchedType == CurrencyPairModel {
	        let matchers: [Cuckoo.ParameterMatcher<(CurrencyPairModel)>] = [wrap(matchable: objCurrencyPair) { $0 }]
	        return .init(stub: cuckoo_manager.createStub(for: MockselectCurrencyInteractor.self, method: "addCurrencyPair(objCurrencyPair: CurrencyPairModel)", parameterMatchers: matchers))
	    }
	    
	    func incrementID() -> Cuckoo.ClassStubFunction<(), Int> {
	        let matchers: [Cuckoo.ParameterMatcher<Void>] = []
	        return .init(stub: cuckoo_manager.createStub(for: MockselectCurrencyInteractor.self, method: "incrementID() -> Int", parameterMatchers: matchers))
	    }
	    
	}

	 struct __VerificationProxy_selectCurrencyInteractor: Cuckoo.VerificationProxy {
	    private let cuckoo_manager: Cuckoo.MockManager
	    private let callMatcher: Cuckoo.CallMatcher
	    private let sourceLocation: Cuckoo.SourceLocation
	
	     init(manager: Cuckoo.MockManager, callMatcher: Cuckoo.CallMatcher, sourceLocation: Cuckoo.SourceLocation) {
	        self.cuckoo_manager = manager
	        self.callMatcher = callMatcher
	        self.sourceLocation = sourceLocation
	    }
	
	    
	    
	    var presenter: Cuckoo.VerifyOptionalProperty<InteractorToPresenterSelectCurrencyProtocol> {
	        return .init(manager: cuckoo_manager, name: "presenter", callMatcher: callMatcher, sourceLocation: sourceLocation)
	    }
	    
	
	    
	    @discardableResult
	    func fetchCurrencyList() -> Cuckoo.__DoNotUse<(), Void> {
	        let matchers: [Cuckoo.ParameterMatcher<Void>] = []
	        return cuckoo_manager.verify("fetchCurrencyList()", callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
	    }
	    
	    @discardableResult
	    func addCurrencyPair<M1: Cuckoo.Matchable>(objCurrencyPair: M1) -> Cuckoo.__DoNotUse<(CurrencyPairModel), Void> where M1.MatchedType == CurrencyPairModel {
	        let matchers: [Cuckoo.ParameterMatcher<(CurrencyPairModel)>] = [wrap(matchable: objCurrencyPair) { $0 }]
	        return cuckoo_manager.verify("addCurrencyPair(objCurrencyPair: CurrencyPairModel)", callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
	    }
	    
	    @discardableResult
	    func incrementID() -> Cuckoo.__DoNotUse<(), Int> {
	        let matchers: [Cuckoo.ParameterMatcher<Void>] = []
	        return cuckoo_manager.verify("incrementID() -> Int", callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
	    }
	    
	}
}

 class selectCurrencyInteractorStub: selectCurrencyInteractor {
    
    
     override var presenter: InteractorToPresenterSelectCurrencyProtocol? {
        get {
            return DefaultValueRegistry.defaultValue(for: (InteractorToPresenterSelectCurrencyProtocol?).self)
        }
        
        set { }
        
    }
    

    

    
     override func fetchCurrencyList()   {
        return DefaultValueRegistry.defaultValue(for: (Void).self)
    }
    
     override func addCurrencyPair(objCurrencyPair: CurrencyPairModel)   {
        return DefaultValueRegistry.defaultValue(for: (Void).self)
    }
    
     override func incrementID() -> Int  {
        return DefaultValueRegistry.defaultValue(for: (Int).self)
    }
    
}


// MARK: - Mocks generated from file: CurrenciesConverter/app-modules/selectCurrency-module/protocols/selectCurrencyProtocol.swift at 2019-07-18 13:59:49 +0000

//
//  ViewToPresenterSelectCurrencyProtocol.swift
//  CurrenciesConverter
//
//  Created by Ashish Patel on 2019/07/13.
//  Copyright © 2019 Ashish Patel. All rights reserved.
//

import Cuckoo
@testable import CurrenciesConverter

import Foundation
import UIKit


 class MockViewToPresenterSelectCurrencyProtocol: ViewToPresenterSelectCurrencyProtocol, Cuckoo.ProtocolMock {
    
     typealias MocksType = ViewToPresenterSelectCurrencyProtocol
    
     typealias Stubbing = __StubbingProxy_ViewToPresenterSelectCurrencyProtocol
     typealias Verification = __VerificationProxy_ViewToPresenterSelectCurrencyProtocol

     let cuckoo_manager = Cuckoo.MockManager.preconfiguredManager ?? Cuckoo.MockManager(hasParent: false)

    
    private var __defaultImplStub: ViewToPresenterSelectCurrencyProtocol?

     func enableDefaultImplementation(_ stub: ViewToPresenterSelectCurrencyProtocol) {
        __defaultImplStub = stub
        cuckoo_manager.enableDefaultStubImplementation()
    }
    

    
    
    
     var view: PresenterToViewSelectCurrencyProtocol? {
        get {
            return cuckoo_manager.getter("view",
                superclassCall:
                    
                    Cuckoo.MockManager.crashOnProtocolSuperclassCall()
                    ,
                defaultCall: __defaultImplStub!.view)
        }
        
        set {
            cuckoo_manager.setter("view",
                value: newValue,
                superclassCall:
                    
                    Cuckoo.MockManager.crashOnProtocolSuperclassCall()
                    ,
                defaultCall: __defaultImplStub!.view = newValue)
        }
        
    }
    
    
    
     var interactor: PresenterToInteractorSelectCurrencyProtocol? {
        get {
            return cuckoo_manager.getter("interactor",
                superclassCall:
                    
                    Cuckoo.MockManager.crashOnProtocolSuperclassCall()
                    ,
                defaultCall: __defaultImplStub!.interactor)
        }
        
        set {
            cuckoo_manager.setter("interactor",
                value: newValue,
                superclassCall:
                    
                    Cuckoo.MockManager.crashOnProtocolSuperclassCall()
                    ,
                defaultCall: __defaultImplStub!.interactor = newValue)
        }
        
    }
    
    
    
     var router: PresenterToRouterSelectCurrencyProtocol? {
        get {
            return cuckoo_manager.getter("router",
                superclassCall:
                    
                    Cuckoo.MockManager.crashOnProtocolSuperclassCall()
                    ,
                defaultCall: __defaultImplStub!.router)
        }
        
        set {
            cuckoo_manager.setter("router",
                value: newValue,
                superclassCall:
                    
                    Cuckoo.MockManager.crashOnProtocolSuperclassCall()
                    ,
                defaultCall: __defaultImplStub!.router = newValue)
        }
        
    }
    

    

    
    
    
     func startFetchingCurrencies()  {
        
    return cuckoo_manager.call("startFetchingCurrencies()",
            parameters: (),
            escapingParameters: (),
            superclassCall:
                
                Cuckoo.MockManager.crashOnProtocolSuperclassCall()
                ,
            defaultCall: __defaultImplStub!.startFetchingCurrencies())
        
    }
    
    
    
     func animateSelectCurrencyController(viewController: UIViewController)  {
        
    return cuckoo_manager.call("animateSelectCurrencyController(viewController: UIViewController)",
            parameters: (viewController),
            escapingParameters: (viewController),
            superclassCall:
                
                Cuckoo.MockManager.crashOnProtocolSuperclassCall()
                ,
            defaultCall: __defaultImplStub!.animateSelectCurrencyController(viewController: viewController))
        
    }
    
    
    
     func addCurrencyPair(objCurrencyPair: CurrencyPairModel)  {
        
    return cuckoo_manager.call("addCurrencyPair(objCurrencyPair: CurrencyPairModel)",
            parameters: (objCurrencyPair),
            escapingParameters: (objCurrencyPair),
            superclassCall:
                
                Cuckoo.MockManager.crashOnProtocolSuperclassCall()
                ,
            defaultCall: __defaultImplStub!.addCurrencyPair(objCurrencyPair: objCurrencyPair))
        
    }
    

	 struct __StubbingProxy_ViewToPresenterSelectCurrencyProtocol: Cuckoo.StubbingProxy {
	    private let cuckoo_manager: Cuckoo.MockManager
	
	     init(manager: Cuckoo.MockManager) {
	        self.cuckoo_manager = manager
	    }
	    
	    
	    var view: Cuckoo.ProtocolToBeStubbedOptionalProperty<MockViewToPresenterSelectCurrencyProtocol, PresenterToViewSelectCurrencyProtocol> {
	        return .init(manager: cuckoo_manager, name: "view")
	    }
	    
	    
	    var interactor: Cuckoo.ProtocolToBeStubbedOptionalProperty<MockViewToPresenterSelectCurrencyProtocol, PresenterToInteractorSelectCurrencyProtocol> {
	        return .init(manager: cuckoo_manager, name: "interactor")
	    }
	    
	    
	    var router: Cuckoo.ProtocolToBeStubbedOptionalProperty<MockViewToPresenterSelectCurrencyProtocol, PresenterToRouterSelectCurrencyProtocol> {
	        return .init(manager: cuckoo_manager, name: "router")
	    }
	    
	    
	    func startFetchingCurrencies() -> Cuckoo.ProtocolStubNoReturnFunction<()> {
	        let matchers: [Cuckoo.ParameterMatcher<Void>] = []
	        return .init(stub: cuckoo_manager.createStub(for: MockViewToPresenterSelectCurrencyProtocol.self, method: "startFetchingCurrencies()", parameterMatchers: matchers))
	    }
	    
	    func animateSelectCurrencyController<M1: Cuckoo.Matchable>(viewController: M1) -> Cuckoo.ProtocolStubNoReturnFunction<(UIViewController)> where M1.MatchedType == UIViewController {
	        let matchers: [Cuckoo.ParameterMatcher<(UIViewController)>] = [wrap(matchable: viewController) { $0 }]
	        return .init(stub: cuckoo_manager.createStub(for: MockViewToPresenterSelectCurrencyProtocol.self, method: "animateSelectCurrencyController(viewController: UIViewController)", parameterMatchers: matchers))
	    }
	    
	    func addCurrencyPair<M1: Cuckoo.Matchable>(objCurrencyPair: M1) -> Cuckoo.ProtocolStubNoReturnFunction<(CurrencyPairModel)> where M1.MatchedType == CurrencyPairModel {
	        let matchers: [Cuckoo.ParameterMatcher<(CurrencyPairModel)>] = [wrap(matchable: objCurrencyPair) { $0 }]
	        return .init(stub: cuckoo_manager.createStub(for: MockViewToPresenterSelectCurrencyProtocol.self, method: "addCurrencyPair(objCurrencyPair: CurrencyPairModel)", parameterMatchers: matchers))
	    }
	    
	}

	 struct __VerificationProxy_ViewToPresenterSelectCurrencyProtocol: Cuckoo.VerificationProxy {
	    private let cuckoo_manager: Cuckoo.MockManager
	    private let callMatcher: Cuckoo.CallMatcher
	    private let sourceLocation: Cuckoo.SourceLocation
	
	     init(manager: Cuckoo.MockManager, callMatcher: Cuckoo.CallMatcher, sourceLocation: Cuckoo.SourceLocation) {
	        self.cuckoo_manager = manager
	        self.callMatcher = callMatcher
	        self.sourceLocation = sourceLocation
	    }
	
	    
	    
	    var view: Cuckoo.VerifyOptionalProperty<PresenterToViewSelectCurrencyProtocol> {
	        return .init(manager: cuckoo_manager, name: "view", callMatcher: callMatcher, sourceLocation: sourceLocation)
	    }
	    
	    
	    var interactor: Cuckoo.VerifyOptionalProperty<PresenterToInteractorSelectCurrencyProtocol> {
	        return .init(manager: cuckoo_manager, name: "interactor", callMatcher: callMatcher, sourceLocation: sourceLocation)
	    }
	    
	    
	    var router: Cuckoo.VerifyOptionalProperty<PresenterToRouterSelectCurrencyProtocol> {
	        return .init(manager: cuckoo_manager, name: "router", callMatcher: callMatcher, sourceLocation: sourceLocation)
	    }
	    
	
	    
	    @discardableResult
	    func startFetchingCurrencies() -> Cuckoo.__DoNotUse<(), Void> {
	        let matchers: [Cuckoo.ParameterMatcher<Void>] = []
	        return cuckoo_manager.verify("startFetchingCurrencies()", callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
	    }
	    
	    @discardableResult
	    func animateSelectCurrencyController<M1: Cuckoo.Matchable>(viewController: M1) -> Cuckoo.__DoNotUse<(UIViewController), Void> where M1.MatchedType == UIViewController {
	        let matchers: [Cuckoo.ParameterMatcher<(UIViewController)>] = [wrap(matchable: viewController) { $0 }]
	        return cuckoo_manager.verify("animateSelectCurrencyController(viewController: UIViewController)", callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
	    }
	    
	    @discardableResult
	    func addCurrencyPair<M1: Cuckoo.Matchable>(objCurrencyPair: M1) -> Cuckoo.__DoNotUse<(CurrencyPairModel), Void> where M1.MatchedType == CurrencyPairModel {
	        let matchers: [Cuckoo.ParameterMatcher<(CurrencyPairModel)>] = [wrap(matchable: objCurrencyPair) { $0 }]
	        return cuckoo_manager.verify("addCurrencyPair(objCurrencyPair: CurrencyPairModel)", callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
	    }
	    
	}
}

 class ViewToPresenterSelectCurrencyProtocolStub: ViewToPresenterSelectCurrencyProtocol {
    
    
     var view: PresenterToViewSelectCurrencyProtocol? {
        get {
            return DefaultValueRegistry.defaultValue(for: (PresenterToViewSelectCurrencyProtocol?).self)
        }
        
        set { }
        
    }
    
    
     var interactor: PresenterToInteractorSelectCurrencyProtocol? {
        get {
            return DefaultValueRegistry.defaultValue(for: (PresenterToInteractorSelectCurrencyProtocol?).self)
        }
        
        set { }
        
    }
    
    
     var router: PresenterToRouterSelectCurrencyProtocol? {
        get {
            return DefaultValueRegistry.defaultValue(for: (PresenterToRouterSelectCurrencyProtocol?).self)
        }
        
        set { }
        
    }
    

    

    
     func startFetchingCurrencies()   {
        return DefaultValueRegistry.defaultValue(for: (Void).self)
    }
    
     func animateSelectCurrencyController(viewController: UIViewController)   {
        return DefaultValueRegistry.defaultValue(for: (Void).self)
    }
    
     func addCurrencyPair(objCurrencyPair: CurrencyPairModel)   {
        return DefaultValueRegistry.defaultValue(for: (Void).self)
    }
    
}



 class MockPresenterToViewSelectCurrencyProtocol: PresenterToViewSelectCurrencyProtocol, Cuckoo.ProtocolMock {
    
     typealias MocksType = PresenterToViewSelectCurrencyProtocol
    
     typealias Stubbing = __StubbingProxy_PresenterToViewSelectCurrencyProtocol
     typealias Verification = __VerificationProxy_PresenterToViewSelectCurrencyProtocol

     let cuckoo_manager = Cuckoo.MockManager.preconfiguredManager ?? Cuckoo.MockManager(hasParent: false)

    
    private var __defaultImplStub: PresenterToViewSelectCurrencyProtocol?

     func enableDefaultImplementation(_ stub: PresenterToViewSelectCurrencyProtocol) {
        __defaultImplStub = stub
        cuckoo_manager.enableDefaultStubImplementation()
    }
    

    

    

    
    
    
     func onSelectCurrencyResponseSuccess(currencyModelArrayList: Array<SelectCurrencyModel>)  {
        
    return cuckoo_manager.call("onSelectCurrencyResponseSuccess(currencyModelArrayList: Array<SelectCurrencyModel>)",
            parameters: (currencyModelArrayList),
            escapingParameters: (currencyModelArrayList),
            superclassCall:
                
                Cuckoo.MockManager.crashOnProtocolSuperclassCall()
                ,
            defaultCall: __defaultImplStub!.onSelectCurrencyResponseSuccess(currencyModelArrayList: currencyModelArrayList))
        
    }
    
    
    
     func onAddCurrencyResponse(result: Bool)  {
        
    return cuckoo_manager.call("onAddCurrencyResponse(result: Bool)",
            parameters: (result),
            escapingParameters: (result),
            superclassCall:
                
                Cuckoo.MockManager.crashOnProtocolSuperclassCall()
                ,
            defaultCall: __defaultImplStub!.onAddCurrencyResponse(result: result))
        
    }
    

	 struct __StubbingProxy_PresenterToViewSelectCurrencyProtocol: Cuckoo.StubbingProxy {
	    private let cuckoo_manager: Cuckoo.MockManager
	
	     init(manager: Cuckoo.MockManager) {
	        self.cuckoo_manager = manager
	    }
	    
	    
	    func onSelectCurrencyResponseSuccess<M1: Cuckoo.Matchable>(currencyModelArrayList: M1) -> Cuckoo.ProtocolStubNoReturnFunction<(Array<SelectCurrencyModel>)> where M1.MatchedType == Array<SelectCurrencyModel> {
	        let matchers: [Cuckoo.ParameterMatcher<(Array<SelectCurrencyModel>)>] = [wrap(matchable: currencyModelArrayList) { $0 }]
	        return .init(stub: cuckoo_manager.createStub(for: MockPresenterToViewSelectCurrencyProtocol.self, method: "onSelectCurrencyResponseSuccess(currencyModelArrayList: Array<SelectCurrencyModel>)", parameterMatchers: matchers))
	    }
	    
	    func onAddCurrencyResponse<M1: Cuckoo.Matchable>(result: M1) -> Cuckoo.ProtocolStubNoReturnFunction<(Bool)> where M1.MatchedType == Bool {
	        let matchers: [Cuckoo.ParameterMatcher<(Bool)>] = [wrap(matchable: result) { $0 }]
	        return .init(stub: cuckoo_manager.createStub(for: MockPresenterToViewSelectCurrencyProtocol.self, method: "onAddCurrencyResponse(result: Bool)", parameterMatchers: matchers))
	    }
	    
	}

	 struct __VerificationProxy_PresenterToViewSelectCurrencyProtocol: Cuckoo.VerificationProxy {
	    private let cuckoo_manager: Cuckoo.MockManager
	    private let callMatcher: Cuckoo.CallMatcher
	    private let sourceLocation: Cuckoo.SourceLocation
	
	     init(manager: Cuckoo.MockManager, callMatcher: Cuckoo.CallMatcher, sourceLocation: Cuckoo.SourceLocation) {
	        self.cuckoo_manager = manager
	        self.callMatcher = callMatcher
	        self.sourceLocation = sourceLocation
	    }
	
	    
	
	    
	    @discardableResult
	    func onSelectCurrencyResponseSuccess<M1: Cuckoo.Matchable>(currencyModelArrayList: M1) -> Cuckoo.__DoNotUse<(Array<SelectCurrencyModel>), Void> where M1.MatchedType == Array<SelectCurrencyModel> {
	        let matchers: [Cuckoo.ParameterMatcher<(Array<SelectCurrencyModel>)>] = [wrap(matchable: currencyModelArrayList) { $0 }]
	        return cuckoo_manager.verify("onSelectCurrencyResponseSuccess(currencyModelArrayList: Array<SelectCurrencyModel>)", callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
	    }
	    
	    @discardableResult
	    func onAddCurrencyResponse<M1: Cuckoo.Matchable>(result: M1) -> Cuckoo.__DoNotUse<(Bool), Void> where M1.MatchedType == Bool {
	        let matchers: [Cuckoo.ParameterMatcher<(Bool)>] = [wrap(matchable: result) { $0 }]
	        return cuckoo_manager.verify("onAddCurrencyResponse(result: Bool)", callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
	    }
	    
	}
}

 class PresenterToViewSelectCurrencyProtocolStub: PresenterToViewSelectCurrencyProtocol {
    

    

    
     func onSelectCurrencyResponseSuccess(currencyModelArrayList: Array<SelectCurrencyModel>)   {
        return DefaultValueRegistry.defaultValue(for: (Void).self)
    }
    
     func onAddCurrencyResponse(result: Bool)   {
        return DefaultValueRegistry.defaultValue(for: (Void).self)
    }
    
}



 class MockPresenterToRouterSelectCurrencyProtocol: PresenterToRouterSelectCurrencyProtocol, Cuckoo.ProtocolMock {
    
     typealias MocksType = PresenterToRouterSelectCurrencyProtocol
    
     typealias Stubbing = __StubbingProxy_PresenterToRouterSelectCurrencyProtocol
     typealias Verification = __VerificationProxy_PresenterToRouterSelectCurrencyProtocol

     let cuckoo_manager = Cuckoo.MockManager.preconfiguredManager ?? Cuckoo.MockManager(hasParent: false)

    
    private var __defaultImplStub: PresenterToRouterSelectCurrencyProtocol?

     func enableDefaultImplementation(_ stub: PresenterToRouterSelectCurrencyProtocol) {
        __defaultImplStub = stub
        cuckoo_manager.enableDefaultStubImplementation()
    }
    

    

    

    
    
    
     func createSelectCurrencyModule(arySelectedCurrenciesPair: Array<CurrencyPairModel>) -> selectCurrencyVC {
        
    return cuckoo_manager.call("createSelectCurrencyModule(arySelectedCurrenciesPair: Array<CurrencyPairModel>) -> selectCurrencyVC",
            parameters: (arySelectedCurrenciesPair),
            escapingParameters: (arySelectedCurrenciesPair),
            superclassCall:
                
                Cuckoo.MockManager.crashOnProtocolSuperclassCall()
                ,
            defaultCall: __defaultImplStub!.createSelectCurrencyModule(arySelectedCurrenciesPair: arySelectedCurrenciesPair))
        
    }
    
    
    
     func animateSelectCurrencyScreen(viewController: UIViewController)  {
        
    return cuckoo_manager.call("animateSelectCurrencyScreen(viewController: UIViewController)",
            parameters: (viewController),
            escapingParameters: (viewController),
            superclassCall:
                
                Cuckoo.MockManager.crashOnProtocolSuperclassCall()
                ,
            defaultCall: __defaultImplStub!.animateSelectCurrencyScreen(viewController: viewController))
        
    }
    

	 struct __StubbingProxy_PresenterToRouterSelectCurrencyProtocol: Cuckoo.StubbingProxy {
	    private let cuckoo_manager: Cuckoo.MockManager
	
	     init(manager: Cuckoo.MockManager) {
	        self.cuckoo_manager = manager
	    }
	    
	    
	    func createSelectCurrencyModule<M1: Cuckoo.Matchable>(arySelectedCurrenciesPair: M1) -> Cuckoo.ProtocolStubFunction<(Array<CurrencyPairModel>), selectCurrencyVC> where M1.MatchedType == Array<CurrencyPairModel> {
	        let matchers: [Cuckoo.ParameterMatcher<(Array<CurrencyPairModel>)>] = [wrap(matchable: arySelectedCurrenciesPair) { $0 }]
	        return .init(stub: cuckoo_manager.createStub(for: MockPresenterToRouterSelectCurrencyProtocol.self, method: "createSelectCurrencyModule(arySelectedCurrenciesPair: Array<CurrencyPairModel>) -> selectCurrencyVC", parameterMatchers: matchers))
	    }
	    
	    func animateSelectCurrencyScreen<M1: Cuckoo.Matchable>(viewController: M1) -> Cuckoo.ProtocolStubNoReturnFunction<(UIViewController)> where M1.MatchedType == UIViewController {
	        let matchers: [Cuckoo.ParameterMatcher<(UIViewController)>] = [wrap(matchable: viewController) { $0 }]
	        return .init(stub: cuckoo_manager.createStub(for: MockPresenterToRouterSelectCurrencyProtocol.self, method: "animateSelectCurrencyScreen(viewController: UIViewController)", parameterMatchers: matchers))
	    }
	    
	}

	 struct __VerificationProxy_PresenterToRouterSelectCurrencyProtocol: Cuckoo.VerificationProxy {
	    private let cuckoo_manager: Cuckoo.MockManager
	    private let callMatcher: Cuckoo.CallMatcher
	    private let sourceLocation: Cuckoo.SourceLocation
	
	     init(manager: Cuckoo.MockManager, callMatcher: Cuckoo.CallMatcher, sourceLocation: Cuckoo.SourceLocation) {
	        self.cuckoo_manager = manager
	        self.callMatcher = callMatcher
	        self.sourceLocation = sourceLocation
	    }
	
	    
	
	    
	    @discardableResult
	    func createSelectCurrencyModule<M1: Cuckoo.Matchable>(arySelectedCurrenciesPair: M1) -> Cuckoo.__DoNotUse<(Array<CurrencyPairModel>), selectCurrencyVC> where M1.MatchedType == Array<CurrencyPairModel> {
	        let matchers: [Cuckoo.ParameterMatcher<(Array<CurrencyPairModel>)>] = [wrap(matchable: arySelectedCurrenciesPair) { $0 }]
	        return cuckoo_manager.verify("createSelectCurrencyModule(arySelectedCurrenciesPair: Array<CurrencyPairModel>) -> selectCurrencyVC", callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
	    }
	    
	    @discardableResult
	    func animateSelectCurrencyScreen<M1: Cuckoo.Matchable>(viewController: M1) -> Cuckoo.__DoNotUse<(UIViewController), Void> where M1.MatchedType == UIViewController {
	        let matchers: [Cuckoo.ParameterMatcher<(UIViewController)>] = [wrap(matchable: viewController) { $0 }]
	        return cuckoo_manager.verify("animateSelectCurrencyScreen(viewController: UIViewController)", callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
	    }
	    
	}
}

 class PresenterToRouterSelectCurrencyProtocolStub: PresenterToRouterSelectCurrencyProtocol {
    

    

    
     func createSelectCurrencyModule(arySelectedCurrenciesPair: Array<CurrencyPairModel>) -> selectCurrencyVC  {
        return DefaultValueRegistry.defaultValue(for: (selectCurrencyVC).self)
    }
    
     func animateSelectCurrencyScreen(viewController: UIViewController)   {
        return DefaultValueRegistry.defaultValue(for: (Void).self)
    }
    
}



 class MockPresenterToInteractorSelectCurrencyProtocol: PresenterToInteractorSelectCurrencyProtocol, Cuckoo.ProtocolMock {
    
     typealias MocksType = PresenterToInteractorSelectCurrencyProtocol
    
     typealias Stubbing = __StubbingProxy_PresenterToInteractorSelectCurrencyProtocol
     typealias Verification = __VerificationProxy_PresenterToInteractorSelectCurrencyProtocol

     let cuckoo_manager = Cuckoo.MockManager.preconfiguredManager ?? Cuckoo.MockManager(hasParent: false)

    
    private var __defaultImplStub: PresenterToInteractorSelectCurrencyProtocol?

     func enableDefaultImplementation(_ stub: PresenterToInteractorSelectCurrencyProtocol) {
        __defaultImplStub = stub
        cuckoo_manager.enableDefaultStubImplementation()
    }
    

    
    
    
     var presenter: InteractorToPresenterSelectCurrencyProtocol? {
        get {
            return cuckoo_manager.getter("presenter",
                superclassCall:
                    
                    Cuckoo.MockManager.crashOnProtocolSuperclassCall()
                    ,
                defaultCall: __defaultImplStub!.presenter)
        }
        
        set {
            cuckoo_manager.setter("presenter",
                value: newValue,
                superclassCall:
                    
                    Cuckoo.MockManager.crashOnProtocolSuperclassCall()
                    ,
                defaultCall: __defaultImplStub!.presenter = newValue)
        }
        
    }
    

    

    
    
    
     func fetchCurrencyList()  {
        
    return cuckoo_manager.call("fetchCurrencyList()",
            parameters: (),
            escapingParameters: (),
            superclassCall:
                
                Cuckoo.MockManager.crashOnProtocolSuperclassCall()
                ,
            defaultCall: __defaultImplStub!.fetchCurrencyList())
        
    }
    
    
    
     func addCurrencyPair(objCurrencyPair: CurrencyPairModel)  {
        
    return cuckoo_manager.call("addCurrencyPair(objCurrencyPair: CurrencyPairModel)",
            parameters: (objCurrencyPair),
            escapingParameters: (objCurrencyPair),
            superclassCall:
                
                Cuckoo.MockManager.crashOnProtocolSuperclassCall()
                ,
            defaultCall: __defaultImplStub!.addCurrencyPair(objCurrencyPair: objCurrencyPair))
        
    }
    

	 struct __StubbingProxy_PresenterToInteractorSelectCurrencyProtocol: Cuckoo.StubbingProxy {
	    private let cuckoo_manager: Cuckoo.MockManager
	
	     init(manager: Cuckoo.MockManager) {
	        self.cuckoo_manager = manager
	    }
	    
	    
	    var presenter: Cuckoo.ProtocolToBeStubbedOptionalProperty<MockPresenterToInteractorSelectCurrencyProtocol, InteractorToPresenterSelectCurrencyProtocol> {
	        return .init(manager: cuckoo_manager, name: "presenter")
	    }
	    
	    
	    func fetchCurrencyList() -> Cuckoo.ProtocolStubNoReturnFunction<()> {
	        let matchers: [Cuckoo.ParameterMatcher<Void>] = []
	        return .init(stub: cuckoo_manager.createStub(for: MockPresenterToInteractorSelectCurrencyProtocol.self, method: "fetchCurrencyList()", parameterMatchers: matchers))
	    }
	    
	    func addCurrencyPair<M1: Cuckoo.Matchable>(objCurrencyPair: M1) -> Cuckoo.ProtocolStubNoReturnFunction<(CurrencyPairModel)> where M1.MatchedType == CurrencyPairModel {
	        let matchers: [Cuckoo.ParameterMatcher<(CurrencyPairModel)>] = [wrap(matchable: objCurrencyPair) { $0 }]
	        return .init(stub: cuckoo_manager.createStub(for: MockPresenterToInteractorSelectCurrencyProtocol.self, method: "addCurrencyPair(objCurrencyPair: CurrencyPairModel)", parameterMatchers: matchers))
	    }
	    
	}

	 struct __VerificationProxy_PresenterToInteractorSelectCurrencyProtocol: Cuckoo.VerificationProxy {
	    private let cuckoo_manager: Cuckoo.MockManager
	    private let callMatcher: Cuckoo.CallMatcher
	    private let sourceLocation: Cuckoo.SourceLocation
	
	     init(manager: Cuckoo.MockManager, callMatcher: Cuckoo.CallMatcher, sourceLocation: Cuckoo.SourceLocation) {
	        self.cuckoo_manager = manager
	        self.callMatcher = callMatcher
	        self.sourceLocation = sourceLocation
	    }
	
	    
	    
	    var presenter: Cuckoo.VerifyOptionalProperty<InteractorToPresenterSelectCurrencyProtocol> {
	        return .init(manager: cuckoo_manager, name: "presenter", callMatcher: callMatcher, sourceLocation: sourceLocation)
	    }
	    
	
	    
	    @discardableResult
	    func fetchCurrencyList() -> Cuckoo.__DoNotUse<(), Void> {
	        let matchers: [Cuckoo.ParameterMatcher<Void>] = []
	        return cuckoo_manager.verify("fetchCurrencyList()", callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
	    }
	    
	    @discardableResult
	    func addCurrencyPair<M1: Cuckoo.Matchable>(objCurrencyPair: M1) -> Cuckoo.__DoNotUse<(CurrencyPairModel), Void> where M1.MatchedType == CurrencyPairModel {
	        let matchers: [Cuckoo.ParameterMatcher<(CurrencyPairModel)>] = [wrap(matchable: objCurrencyPair) { $0 }]
	        return cuckoo_manager.verify("addCurrencyPair(objCurrencyPair: CurrencyPairModel)", callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
	    }
	    
	}
}

 class PresenterToInteractorSelectCurrencyProtocolStub: PresenterToInteractorSelectCurrencyProtocol {
    
    
     var presenter: InteractorToPresenterSelectCurrencyProtocol? {
        get {
            return DefaultValueRegistry.defaultValue(for: (InteractorToPresenterSelectCurrencyProtocol?).self)
        }
        
        set { }
        
    }
    

    

    
     func fetchCurrencyList()   {
        return DefaultValueRegistry.defaultValue(for: (Void).self)
    }
    
     func addCurrencyPair(objCurrencyPair: CurrencyPairModel)   {
        return DefaultValueRegistry.defaultValue(for: (Void).self)
    }
    
}



 class MockInteractorToPresenterSelectCurrencyProtocol: InteractorToPresenterSelectCurrencyProtocol, Cuckoo.ProtocolMock {
    
     typealias MocksType = InteractorToPresenterSelectCurrencyProtocol
    
     typealias Stubbing = __StubbingProxy_InteractorToPresenterSelectCurrencyProtocol
     typealias Verification = __VerificationProxy_InteractorToPresenterSelectCurrencyProtocol

     let cuckoo_manager = Cuckoo.MockManager.preconfiguredManager ?? Cuckoo.MockManager(hasParent: false)

    
    private var __defaultImplStub: InteractorToPresenterSelectCurrencyProtocol?

     func enableDefaultImplementation(_ stub: InteractorToPresenterSelectCurrencyProtocol) {
        __defaultImplStub = stub
        cuckoo_manager.enableDefaultStubImplementation()
    }
    

    

    

    
    
    
     func selectCurrencyFetchSuccess(currenciesList: Array<SelectCurrencyModel>)  {
        
    return cuckoo_manager.call("selectCurrencyFetchSuccess(currenciesList: Array<SelectCurrencyModel>)",
            parameters: (currenciesList),
            escapingParameters: (currenciesList),
            superclassCall:
                
                Cuckoo.MockManager.crashOnProtocolSuperclassCall()
                ,
            defaultCall: __defaultImplStub!.selectCurrencyFetchSuccess(currenciesList: currenciesList))
        
    }
    
    
    
     func addCurrencyPairSuccess(result: Bool)  {
        
    return cuckoo_manager.call("addCurrencyPairSuccess(result: Bool)",
            parameters: (result),
            escapingParameters: (result),
            superclassCall:
                
                Cuckoo.MockManager.crashOnProtocolSuperclassCall()
                ,
            defaultCall: __defaultImplStub!.addCurrencyPairSuccess(result: result))
        
    }
    

	 struct __StubbingProxy_InteractorToPresenterSelectCurrencyProtocol: Cuckoo.StubbingProxy {
	    private let cuckoo_manager: Cuckoo.MockManager
	
	     init(manager: Cuckoo.MockManager) {
	        self.cuckoo_manager = manager
	    }
	    
	    
	    func selectCurrencyFetchSuccess<M1: Cuckoo.Matchable>(currenciesList: M1) -> Cuckoo.ProtocolStubNoReturnFunction<(Array<SelectCurrencyModel>)> where M1.MatchedType == Array<SelectCurrencyModel> {
	        let matchers: [Cuckoo.ParameterMatcher<(Array<SelectCurrencyModel>)>] = [wrap(matchable: currenciesList) { $0 }]
	        return .init(stub: cuckoo_manager.createStub(for: MockInteractorToPresenterSelectCurrencyProtocol.self, method: "selectCurrencyFetchSuccess(currenciesList: Array<SelectCurrencyModel>)", parameterMatchers: matchers))
	    }
	    
	    func addCurrencyPairSuccess<M1: Cuckoo.Matchable>(result: M1) -> Cuckoo.ProtocolStubNoReturnFunction<(Bool)> where M1.MatchedType == Bool {
	        let matchers: [Cuckoo.ParameterMatcher<(Bool)>] = [wrap(matchable: result) { $0 }]
	        return .init(stub: cuckoo_manager.createStub(for: MockInteractorToPresenterSelectCurrencyProtocol.self, method: "addCurrencyPairSuccess(result: Bool)", parameterMatchers: matchers))
	    }
	    
	}

	 struct __VerificationProxy_InteractorToPresenterSelectCurrencyProtocol: Cuckoo.VerificationProxy {
	    private let cuckoo_manager: Cuckoo.MockManager
	    private let callMatcher: Cuckoo.CallMatcher
	    private let sourceLocation: Cuckoo.SourceLocation
	
	     init(manager: Cuckoo.MockManager, callMatcher: Cuckoo.CallMatcher, sourceLocation: Cuckoo.SourceLocation) {
	        self.cuckoo_manager = manager
	        self.callMatcher = callMatcher
	        self.sourceLocation = sourceLocation
	    }
	
	    
	
	    
	    @discardableResult
	    func selectCurrencyFetchSuccess<M1: Cuckoo.Matchable>(currenciesList: M1) -> Cuckoo.__DoNotUse<(Array<SelectCurrencyModel>), Void> where M1.MatchedType == Array<SelectCurrencyModel> {
	        let matchers: [Cuckoo.ParameterMatcher<(Array<SelectCurrencyModel>)>] = [wrap(matchable: currenciesList) { $0 }]
	        return cuckoo_manager.verify("selectCurrencyFetchSuccess(currenciesList: Array<SelectCurrencyModel>)", callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
	    }
	    
	    @discardableResult
	    func addCurrencyPairSuccess<M1: Cuckoo.Matchable>(result: M1) -> Cuckoo.__DoNotUse<(Bool), Void> where M1.MatchedType == Bool {
	        let matchers: [Cuckoo.ParameterMatcher<(Bool)>] = [wrap(matchable: result) { $0 }]
	        return cuckoo_manager.verify("addCurrencyPairSuccess(result: Bool)", callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
	    }
	    
	}
}

 class InteractorToPresenterSelectCurrencyProtocolStub: InteractorToPresenterSelectCurrencyProtocol {
    

    

    
     func selectCurrencyFetchSuccess(currenciesList: Array<SelectCurrencyModel>)   {
        return DefaultValueRegistry.defaultValue(for: (Void).self)
    }
    
     func addCurrencyPairSuccess(result: Bool)   {
        return DefaultValueRegistry.defaultValue(for: (Void).self)
    }
    
}

